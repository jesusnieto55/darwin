﻿using Xunit;

namespace TravellingSalesmanProblem.Tests
{
    public class FitnessCalculatorTests
    {
        private readonly TSPFitnessCalculator fitnessCalculator;

        public FitnessCalculatorTests()
        {
            this.fitnessCalculator = new TSPFitnessCalculator();
        }

        [Fact]
        public void OneCities_ShouldReturnZero()
        {
            // Arrange
            var genotype = new City[]
            {
                new City { X = 1, Y = 2 }
            };

            // Act
            var fitness = this.fitnessCalculator.CalculateFitness(new TSPSolution { Items = genotype });

            // Assert
            Assert.Equal(0, fitness);
        }

        [Fact]
        public void TwoCities_ShouldReturnDistance()
        {
            // Arrange
            var genotype = new City[]
            {
                new City { X = 1, Y = 2 },
                new City { X = 2, Y = 1 }
            };

            // Act
            var fitness = this.fitnessCalculator.CalculateFitness(new TSPSolution { Items = genotype });

            // Assert
            Assert.Equal(3535.5339059327375, fitness);
        }

        [Fact]
        public void ThreeCities_ShouldReturnDistance()
        {
            // Arrange
            var genotype = new City[]
            {
                new City { X = 1, Y = 2 },
                new City { X = 2, Y = 1 },
                new City { X = 4, Y = 3 }
            };

            // Act
            var fitness = this.fitnessCalculator.CalculateFitness(new TSPSolution { Items = genotype });

            // Assert
            Assert.Equal(1350.453783688632, fitness);
        }

        [Fact]
        public void TenCities_ShouldReturnDistance()
        {
            // Arrange
            var genotype = new City[]
            {
                new City { X = 1, Y = 2 },
                new City { X = 2, Y = 1 },
                new City { X = 4, Y = 3 },
                new City { X = 5, Y = 1 },
                new City { X = 2, Y = 0 },
                new City { X = 4, Y = 4 },
                new City { X = 8, Y = 5 },
                new City { X = 12, Y = 1 },
                new City { X = 2, Y = 2 },
                new City { X = 1, Y = 13 }
            };

            // Act
            var fitness = this.fitnessCalculator.CalculateFitness(new TSPSolution { Items = genotype });

            // Assert
            Assert.Equal(178.60868508903334, fitness);
        }
    }
}
