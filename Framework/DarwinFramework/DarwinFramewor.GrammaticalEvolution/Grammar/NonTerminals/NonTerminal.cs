﻿namespace DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals
{
    public interface INonTerminal : IGrammarElement
    {
        string Key { get; }
    }
}
