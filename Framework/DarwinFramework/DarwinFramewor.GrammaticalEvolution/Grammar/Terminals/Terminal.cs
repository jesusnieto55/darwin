﻿using System.Collections.Generic;

namespace DarwinFramewor.GrammaticalEvolution.Grammar.Terminals
{
    public interface ITerminal : IGrammarElement
    {
        string Key { get; }
    }
}
