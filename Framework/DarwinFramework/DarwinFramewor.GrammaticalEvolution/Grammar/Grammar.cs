﻿using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;
using DarwinFramewor.GrammaticalEvolution.Grammar.Rules;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramewor.GrammaticalEvolution.Grammar
{
    public class Grammar<T>
    {
        protected int codonIndex = 0;
        private int wrapping;

        public List<IRule> Rules { get; set; }
        public INonTerminal StartSymbol { get; set; }

        public Grammar(int wrapping)
        {
            this.wrapping = wrapping;
        }

        public bool IsValid(List<int> codons)
        {
            this.codonIndex = 0;
            var printed = PrintStatement(this.StartSymbol, codons);
            return printed != null;
        }

        public string PrintExpression(List<int> codons)
        {
            if (!codons.Any())
            {
                return string.Empty;
            }

            this.codonIndex = 0;
            return PrintStatement(this.StartSymbol, codons);
        }

        private string PrintStatement(IGrammarElement grammarElement, List<int> codons)
        {
            if (grammarElement is ITerminal)
            {
                return (grammarElement as ITerminal).Key;
            }
            var nonTerminal = grammarElement as INonTerminal;

            var codon = this.GetCodon(codons);
            if (!codon.HasValue)
            {
                return null;
            }
            var rule = this.GetRule(nonTerminal, codon.Value);
            var statements = rule.Apply();
            var result = string.Empty;

            foreach(var statement in statements)
            {
                var printedStatement = PrintStatement(statement, codons);
                if (printedStatement == null)
                {
                    return null;
                }
                result += printedStatement;
            }

            return result;
        }

        protected IRule GetRule(INonTerminal nonTerminal, int codon)
        {
            var applicableRules = this.Rules.Where(r => r.CanHandle(nonTerminal)).ToList();
            var index = codon % applicableRules.Count;
            if (index < 0)
                index = -1 * index;
            return applicableRules[index];
        }

        protected int? GetCodon(List<int> codons)
        {
            if (this.codonIndex > (this.wrapping * codons.Count))
            {
                return null;
            }
            var index = this.codonIndex % codons.Count;
            var codon = codons[index];
            this.codonIndex++;
            return codon;
        }
    }
}
