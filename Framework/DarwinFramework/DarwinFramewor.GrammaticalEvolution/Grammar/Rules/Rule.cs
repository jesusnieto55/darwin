﻿using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;
using System.Collections.Generic;

namespace DarwinFramewor.GrammaticalEvolution.Grammar.Rules
{
    public interface IRule
    {
        bool CanHandle(INonTerminal nonTerminal);
        List<IGrammarElement> Apply();
    }
}
