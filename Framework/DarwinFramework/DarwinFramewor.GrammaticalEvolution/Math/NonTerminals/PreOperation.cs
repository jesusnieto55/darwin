﻿using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;

namespace DarwinFramework.GrammaticalEvolution.Math.NonTerminals
{
    public class PreOperation : INonTerminal
    {
        public string Key => "pre_op";
    }
}
