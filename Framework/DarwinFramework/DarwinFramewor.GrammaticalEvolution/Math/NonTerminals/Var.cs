﻿using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;

namespace DarwinFramework.GrammaticalEvolution.Math.NonTerminals
{
    public class Var : INonTerminal
    {
        public string Key => "var";
    }
}
