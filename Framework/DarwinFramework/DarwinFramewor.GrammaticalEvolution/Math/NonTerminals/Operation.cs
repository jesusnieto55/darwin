﻿using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;

namespace DarwinFramework.GrammaticalEvolution.Math.NonTerminals
{
    public class Operation : INonTerminal
    {
        public string Key => "op";
    }
}
