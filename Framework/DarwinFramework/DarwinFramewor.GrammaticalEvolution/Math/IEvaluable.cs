﻿using System.Collections.Generic;

namespace DarwinFramework.GrammaticalEvolution.Math
{
    public interface IEvaluable<T>
    {
        T Evaluate(T input);
    }
}
