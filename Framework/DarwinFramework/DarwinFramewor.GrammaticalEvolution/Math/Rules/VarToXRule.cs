﻿using DarwinFramewor.GrammaticalEvolution.Grammar;
using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;
using DarwinFramewor.GrammaticalEvolution.Grammar.Rules;
using DarwinFramework.GrammaticalEvolution.Math.NonTerminals;
using DarwinFramework.GrammaticalEvolution.Math.Terminals;
using System.Collections.Generic;

namespace DarwinFramework.GrammaticalEvolution.Math.Rules
{
    public class VarToXRule : IRule
    {
        public bool CanHandle(INonTerminal nonTerminal)
        {
            return nonTerminal is Var;
        }

        public List<IGrammarElement> Apply()
        {
            return new List<IGrammarElement>() { new XTerminal() };
        }
    }
}
