﻿using DarwinFramewor.GrammaticalEvolution.Grammar;
using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;
using DarwinFramewor.GrammaticalEvolution.Grammar.Rules;
using DarwinFramework.GrammaticalEvolution.Math.NonTerminals;
using System.Collections.Generic;

namespace DarwinFramework.GrammaticalEvolution.Math.Rules
{
    public class ExprToExprOpExprRule : IRule
    {
        public bool CanHandle(INonTerminal nonTerminal)
        {
            return nonTerminal is Expression;
        }

        public List<IGrammarElement> Apply()
        {
            return new List<IGrammarElement>
            {
                new Expression(),
                new Operation(),
                new Expression()
            };
        }
    }
}
