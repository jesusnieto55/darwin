﻿using DarwinFramewor.GrammaticalEvolution.Grammar;
using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;
using DarwinFramewor.GrammaticalEvolution.Grammar.Rules;
using DarwinFramework.GrammaticalEvolution.Math.NonTerminals;
using DarwinFramework.GrammaticalEvolution.Math.Terminals;
using System.Collections.Generic;

namespace DarwinFramework.GrammaticalEvolution.Math.Rules
{
    public class PreOpToCosRule : IRule
    {
        public List<IGrammarElement> Apply()
        {
            return new List<IGrammarElement> { new CosTerminal() };
        }

        public bool CanHandle(INonTerminal nonTerminal)
        {
            return nonTerminal is PreOperation;
        }
    }
}
