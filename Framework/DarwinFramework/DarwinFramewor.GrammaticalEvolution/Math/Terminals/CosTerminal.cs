﻿using System.Collections.Generic;
using System.Linq;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class CosTerminal : ITerminal, IOperation<double>
    {
        public string Key => "cos";

        public bool IsValid(List<double> inputs)
        {
            return inputs.Any();
        }

        public double Operate(List<double> input)
        {
            return System.Math.Cos(input.First());
        }
    }
}
