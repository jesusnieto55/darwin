﻿using System.Collections.Generic;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class DivisionTerminal : ITerminal, IOperation<double>
    {
        public string Key => "/";

        public bool IsValid(List<double> inputs)
        {
            return inputs.Count > 1 && inputs[1] != 0;
        }

        public double Operate(List<double> input)
        {
            return input[0] / input[1];
        }
    }
}
