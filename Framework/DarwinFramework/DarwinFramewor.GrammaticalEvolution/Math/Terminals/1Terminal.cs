﻿using System.Collections.Generic;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class _1Terminal : ITerminal, IEvaluable<double>
    {
        public string Key => "1";

        public double Evaluate(double input)
        {
            return 1;
        }
    }
}
