﻿using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class CloseParenthesisTerminal : ITerminal
    {
        public string Key => ")";
    }
}
