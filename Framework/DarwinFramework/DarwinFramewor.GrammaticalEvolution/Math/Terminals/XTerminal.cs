﻿using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class XTerminal : ITerminal, IEvaluable<double>
    {
        public string Key => "x";

        public double Evaluate(double input)
        {
            return input;
        }
    }
}
