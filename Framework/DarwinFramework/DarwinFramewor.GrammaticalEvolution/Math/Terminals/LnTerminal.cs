﻿using System.Collections.Generic;
using System.Linq;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class LnTerminal : ITerminal, IOperation<double>
    {
        public string Key => "ln";

        public bool IsValid(List<double> inputs)
        {
            return inputs.Any() && inputs.First() > 0;
        }

        public double Operate(List<double> input)
        {
            return System.Math.Log(input.First());
        }
    }
}
