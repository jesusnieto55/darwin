﻿using System.Collections.Generic;
using System.Linq;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class SinTerminal : ITerminal, IOperation<double>
    {
        public string Key => "sin";

        public bool IsValid(List<double> inputs)
        {
            return inputs.Any();
        }

        public double Operate(List<double> input)
        {
            return System.Math.Sin(input.First());
        }
    }
}
