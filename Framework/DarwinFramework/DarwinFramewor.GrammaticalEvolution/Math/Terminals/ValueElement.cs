﻿using DarwinFramewor.GrammaticalEvolution.Grammar;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class ValueElement : IGrammarElement, IEvaluable<double>
    {
        private readonly double value;

        public ValueElement(double value)
        {
            this.value = value;
        }

        public double Evaluate(double input)
        {
            return this.value;
        }
    }
}
