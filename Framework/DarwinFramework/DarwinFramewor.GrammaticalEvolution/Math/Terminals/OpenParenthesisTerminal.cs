﻿using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;

namespace DarwinFramework.GrammaticalEvolution.Math.Terminals
{
    public class OpenParenthesisTerminal : ITerminal
    {
        public string Key => "(";
    }
}
