﻿using System.Collections.Generic;

namespace DarwinFramework.GrammaticalEvolution.Math
{
    public interface IOperation<T>
    {
        T Operate(List<T> inputs);
        bool IsValid(List<T> inputs);
    }
}
