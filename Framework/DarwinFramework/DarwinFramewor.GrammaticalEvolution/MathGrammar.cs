﻿using DarwinFramewor.GrammaticalEvolution.Grammar;
using DarwinFramewor.GrammaticalEvolution.Grammar.NonTerminals;
using DarwinFramewor.GrammaticalEvolution.Grammar.Rules;
using DarwinFramewor.GrammaticalEvolution.Grammar.Terminals;
using DarwinFramework.GrammaticalEvolution.Math;
using DarwinFramework.GrammaticalEvolution.Math.NonTerminals;
using DarwinFramework.GrammaticalEvolution.Math.Rules;
using DarwinFramework.GrammaticalEvolution.Math.Terminals;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.GrammaticalEvolution
{
    public class MathGrammar : Grammar<double>
    {
        public MathGrammar(int wrapping) : base(wrapping)
        {
            this.Rules = new List<IRule>
            {
                new ExprToExprOpExprRule(),
                new ExprToExprOpExprWithParenthesisRule(),
                new ExprToPreOpRule(),
                new ExprToVarRule(),
                new OperationToSumRule(),
                new OperationToMinusRule(),
                new OperationToProductRule(),
                new OperationToDivisionRule(),
                new PreOpToSinRule(),
                new PreOpToCosRule(),
                new PreOpToExpRule(),
                new PreOpToLnRule(),
                new VarToXRule(),
                new VarTo1Rule()
            };
            this.StartSymbol = new Expression();
        }

        public double? Evaluate(List<int> codons, double input)
        {
            if (!codons.Any())
            {
                return 0;
            }
            this.codonIndex = 0;
            var expression = EvaluateStatement(this.StartSymbol, codons, input) as IEvaluable<double>;
            return expression?.Evaluate(input);
        }

        private IGrammarElement EvaluateStatement(IGrammarElement grammarElement, List<int> codons, double input)
        {
            if (grammarElement is ITerminal)
            {
                return grammarElement;
            }
            var nonTerminal = grammarElement as INonTerminal;

            var codon = this.GetCodon(codons);
            if (!codon.HasValue)
            {
                return null;
            }
            var rule = this.GetRule(nonTerminal, codon.Value);
            var statements = rule.Apply();

            var evaluatedStatements = statements.Select(s => EvaluateStatement(s, codons, input)).ToList();
            var operationStatement = evaluatedStatements?.Where(s => s is IOperation<double>).FirstOrDefault();
            var evaluables = evaluatedStatements?.Select(s => s as IEvaluable<double>).Where(s => s != null).ToList();

            if (evaluatedStatements.Any(e => e == null))
            {
                return null;
            }

            if (operationStatement != null)
            {
                if (!evaluables.Any())
                {
                    return operationStatement;
                }
                var values = evaluables.Select(e => e.Evaluate(input)).ToList();
                var operation = (operationStatement as IOperation<double>);

                if (!operation.IsValid(values))
                {
                    return null;
                }

                var operationResult = operation.Operate(values);
                return new ValueElement(operationResult);
            }

            var value = evaluables.FirstOrDefault().Evaluate(input);
            return new ValueElement(value);
        }
    }
}
