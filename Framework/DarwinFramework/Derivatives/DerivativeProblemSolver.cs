﻿using DarwinFramework;
using DarwinFramework.GrammaticalEvolution;
using DarwinFramework.LocalSearch;
using DarwinFramework.Mutation;
using DarwinFramework.Mutation.MutationRate;
using DarwinFramework.OffspringGeneration;
using DarwinFramework.ParentSelector;
using DarwinFramework.Recombination;
using DarwinFramework.SurvivorSelector;
using DarwinFramework.Termination;
using DarwinFramework.Utils;
using Derivatives.Functions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Derivatives
{
    public class DerivativeProblemSolver
    {
        public GeneticAlgorithm<DerivativeSolution> GeneticAlgorithm { get; private set; }
        private DerivativeFitnessCalculator fitnessCalculator;
        private readonly RandomGenerator randomGenerator;
        private MathGrammar mathGrammar;
        private Settings settings;

        // Settings
        private int initialPopulationSize = 60;
        private int maxRemainingLife = 5;
        private int maxCodonLength = 100;
        private int tournamentSize = 50;
        private int crossoverPoints;

        private int wrapping = 3;
        private int numberOfParents = 50;
        private double crossoverRate;
        private int maxGenerations = 1000;

        public DerivativeProblemSolver()
        {
            this.randomGenerator = new RandomGenerator();
        }

        public void Solve(Options options)
        {
            this.settings = new Settings
            {
                SaveHistory = false,
                CrossoverRate = this.crossoverRate,
                NumberOfParents = this.numberOfParents
            };
            this.mathGrammar = new MathGrammar(this.wrapping);
            this.crossoverPoints = options.CrossoverPoints;
            this.crossoverRate = options.CrossoverRate;

            var population = this.GenerateRandomPopulation(options);
            var function = this.GetFunction(options);
            this.fitnessCalculator = new DerivativeFitnessCalculator(this.mathGrammar, function);

            var tournamentSelectionSettings = new TournamentSelectionSettings { TournamentSize = this.tournamentSize };

            
            var mutationStrategy = this.GetMutationStrategy(options);
            var recombinationStrategy = this.GetRecombinationStrategy(options);
            var crossoverRateStrategy = this.GetCrossoverRateStrategy(options); ;
            var offspringGenerationStrategy = new SexualOffspringGenerationStrategy<DerivativeSolution>(this.randomGenerator, recombinationStrategy, crossoverRateStrategy);
            var survivorSelectorStrategy = new LifetimeBasedSurvivorSelector<DerivativeSolution>(fitnessCalculator, this.maxRemainingLife);
            var parentSelectorStrategy = new TournamentSelection<DerivativeSolution>(tournamentSelectionSettings, this.randomGenerator);
            var terminationStrategy = new SolutionReachedTerminationStrategy<DerivativeSolution>(this.maxGenerations, 0, 0);

            var offspringImprover = this.GetOffspringImprover(options, fitnessCalculator);

            this.GeneticAlgorithm = new GeneticAlgorithm<DerivativeSolution>(
                mutationStrategy,
                offspringGenerationStrategy,
                terminationStrategy,
                survivorSelectorStrategy,
                parentSelectorStrategy,
                fitnessCalculator,
                settings,
                offspringImprover);

            var testRun = new GeneticAlgorithmTestRun<DerivativeSolution>(this.GeneticAlgorithm, options.Runs);
            testRun.Execute(population, options.Name);

            this.PrintSolution();
        }

        private ICrossoverRateStrategy<DerivativeSolution> GetCrossoverRateStrategy(Options options)
        {
            if (options.CrossoverRate != 0)
            {
                return new FixedCrossoverRate<DerivativeSolution>(options.CrossoverRate);
            }
            return new FitnessBasedCrossoverRate<DerivativeSolution>(this.fitnessCalculator);
        }

        private IOffspringImprover<DerivativeSolution> GetOffspringImprover(Options options, DerivativeFitnessCalculator fitnessCalculator)
        {
            if (options.UseDuplication)
            {
                return new GeneDuplication<DerivativeSolution, int>(this.randomGenerator);
            }
            if (options.UseLocalSearch)
            {
                return new DerivativeOffspringImprover(fitnessCalculator, this.randomGenerator);
            }
            return null;
        }

        private IFunction GetFunction(Options options)
        {
            switch (options.Function)
            {
                case 1:
                    return new Function1();
                case 2:
                    return new Function2();
                case 3:
                    return new Function3();
                case 4:
                    return new Function4();
                case 5:
                    return new Function5();
                case 6:
                    return new Function6();
                case 7:
                    return new Function7();
                default:
                    throw new ArgumentException("Function parameter not provided");
            }
        }

        private IMutationStrategy<DerivativeSolution> GetMutationStrategy(Options options)
        {
            var mutationRateStrategy = new SelfAdaptiveMutationRateStrategy<DerivativeSolution>(this.randomGenerator);

            switch (options.Mutation)
            {
                case Mutation.RandomResseting:
                    return new RandomResettingMutationStrategy<DerivativeSolution>(mutationRateStrategy, this.randomGenerator, 3);
                case Mutation.CreepMutation:
                    var normalDistribution = new NormalDistribution(this.randomGenerator);
                    return new CreepMutationStrategy<DerivativeSolution>(mutationRateStrategy, normalDistribution, this.randomGenerator);
                default:
                    throw new ArgumentException("Mutation not valid");
            }
        }

        private IRecombinationStrategy<DerivativeSolution> GetRecombinationStrategy(Options options)
        {
            switch (options.Crossover)
            {
                case Crossover.NPoint:
                    return new NPointCrossover<DerivativeSolution, int>(this.randomGenerator, this.crossoverPoints);
                case Crossover.Uniform:
                    return new UniformCrossover<DerivativeSolution, int>(this.randomGenerator);
                default:
                    throw new ArgumentException("Crossover not valid");
            }
        } 

        private void PrintSolution()
        {
            var bestSolution = this.GeneticAlgorithm.Population.GetBestIndividual();
            Console.WriteLine($"Function {-1 * bestSolution.FitnessValue}: {this.mathGrammar.PrintExpression(bestSolution.Individual.Items.ToList())}");
            Console.WriteLine($"Number of generations: {this.GeneticAlgorithm.NumberOfGenerations}");
            Console.WriteLine($"Execution time: {this.GeneticAlgorithm.History.ExecutionTime} miliseconds");
        }

        private IEnumerable<DerivativeSolution> GenerateRandomPopulation(Options options)
        {
            var population = new List<DerivativeSolution>();
            var minCodonLenght = options.Crossover == Crossover.Uniform ? this.maxCodonLength : 1;

            for (var i = 0; i < this.initialPopulationSize; i++)
            {
                var randomIndividual = new DerivativeSolution
                {
                    Items = Enumerable.Range(0, this.randomGenerator.Generate(minCodonLenght, this.maxCodonLength))
                            .Select(x => this.randomGenerator.Generate(0, 4)).ToArray(),
                    EncodedMutationRate = Enumerable.Range(0, 10)
                            .Select(x => this.randomGenerator.Generate(0, 1) == 1).ToArray(),
                    RemainingLifetime = this.randomGenerator.Generate(0, 3)
                };
                population.Add(randomIndividual);
            }

            return population;
        }
    }
}
