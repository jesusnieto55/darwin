﻿using DarwinFramework;
using DarwinFramework.GrammaticalEvolution;
using DarwinFramework.LocalSearch;
using DarwinFramework.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Derivatives
{
    public class DerivativeOffspringImprover : IOffspringImprover<DerivativeSolution>
    {
        private readonly FitnessCalculator<DerivativeSolution> fitnessCalculator;
        private readonly RandomGenerator randomGenerator;

        public DerivativeOffspringImprover(FitnessCalculator<DerivativeSolution> fitnessCalculator, RandomGenerator randomGenerator)
        {
            this.fitnessCalculator = fitnessCalculator;
            this.randomGenerator = randomGenerator;
        }

        private List<DerivativeSolution> ImproveOffspringByBruteForce(Population<DerivativeSolution> population, List<DerivativeSolution> offspring)
        {
            var bestIndividual = population.GetBestIndividual();

            var randomPosition1 = (int)new NormalDistribution(this.randomGenerator).Generate(bestIndividual.Individual.Items.Length / 3, bestIndividual.Individual.Items.Length / 5);
            if (randomPosition1 < 0)
                randomPosition1 = 0;
            if (randomPosition1 >= bestIndividual.Individual.Items.Length)
                randomPosition1 = bestIndividual.Individual.Items.Length - 1;

            var candidates =
                this.GetCombinations<int>(Enumerable.Range(0, 4), 4)
                .Select(c =>
                {
                    var items = bestIndividual.Individual.Items.ToList();
                    items.InsertRange(randomPosition1, c);
                    return new DerivativeSolution
                    {
                        Items = items.ToArray(),
                        EncodedMutationRate = bestIndividual.Individual.EncodedMutationRate
                    };
                }).ToList();

            var improvedOffspring = candidates.Where(c => this.fitnessCalculator.CalculateFitness(c) > bestIndividual.FitnessValue).ToList();

            if (improvedOffspring.Any())
            {
                System.Console.WriteLine($"Improved offspring with {improvedOffspring.Count} individuals");
                offspring.AddRange(improvedOffspring);
            }


            return offspring;
        }

        public List<DerivativeSolution> ImproveOffspring(Population<DerivativeSolution> population, List<DerivativeSolution> offspring)
        {
            return this.ImproveOffspringByBruteForce(population, offspring);
        }

        private IEnumerable<IEnumerable<T>> GetCombinations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetCombinations(list, length - 1)
                .SelectMany(t => list, (t1, t2) => t1.Concat(new T[] { t2 }));
        }
    }
}
