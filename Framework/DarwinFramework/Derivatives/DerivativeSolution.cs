﻿using DarwinFramework;
using DarwinFramework.GrammaticalEvolution;
using DarwinFramework.Individual;
using System.Linq;

namespace Derivatives
{
    public class DerivativeSolution : SelfAdaptiveMutationRateIndividual, ICloneable<DerivativeSolution>, IPermutation<int>, ILifetimeIndividual
    {
        // This represents the codons
        public int[] Items { get; set; }
        public int? RemainingLifetime { get; set; }

        public DerivativeSolution Clone()
        {
            return new DerivativeSolution
            {
                Items = Items.Select(i => i).ToArray(),
                EncodedMutationRate = EncodedMutationRate.Select(i => i).ToArray()
            };
        }

        public override string ToString()
        {
            var mathGrammar = new MathGrammar(3);
            return mathGrammar.PrintExpression(this.Items.ToList());
        }
    }
}
