﻿using DarwinFramework;
using DarwinFramework.GrammaticalEvolution;
using Derivatives.Functions;
using System.Linq;

namespace Derivatives
{
    public class DerivativeFitnessCalculator : FitnessCalculator<DerivativeSolution>
    {
        private readonly MathGrammar mathGrammar;
        private readonly IFunction function;
        private readonly double n = 50;
        private readonly double h = 0.00001;
        private readonly double u = 0.1;
        private readonly double k0 = 1;
        private readonly double k1 = 10;
        private readonly double penaltyWeight = 50;
        private readonly double minFitness = -10000;


        public DerivativeFitnessCalculator(MathGrammar mathGrammar, IFunction function)
        {
            this.mathGrammar = mathGrammar;
            this.function = function;
        }

        public override double GetFitness(DerivativeSolution individual)
        {
            var isValid = this.mathGrammar.IsValid(individual.Items.ToList());
            if (!isValid)
            {
                // If codons are not valid, we return a very low fitness
                return 2*this.minFitness;
            }

            var rulesViolated = 0;
            var error = 0.0;
            var hits = 0;
            var delta = (this.function.DomainEnd - this.function.DomainStart) / this.n;

            for (double i = 0; i <= this.n; i++)
            {
                var x = this.function.DomainStart + (i *delta);
                var approximatedValue = ApproximateDerivative(x);
                var candidateValue = this.mathGrammar.Evaluate(individual.Items.ToList(), x);
                if (!this.IsValid(candidateValue))
                {
                    rulesViolated++;
                }
                else
                {
                    var absError = System.Math.Abs(approximatedValue - candidateValue.Value);
                    var w = this.GetW(absError);
                    var isHit = this.IsHit(absError);
                    if (isHit)
                    {
                        hits++;
                    }
                    error += w * absError;
                }
            }

            var totalError = error * (1 / (this.n + 1));
            totalError += this.penaltyWeight * rulesViolated;
            totalError *= -1;
            if (hits == this.n + 1)
            {
                return 0;
            }
            return totalError < this.minFitness ? this.minFitness : totalError;
        }

        private bool IsValid(double? candidate)
        {
            return candidate.HasValue && !double.IsNaN(candidate.Value) && !double.IsNegativeInfinity(candidate.Value) && !double.IsInfinity(candidate.Value);
        }

        private double ApproximateDerivative(double x)
        {
            return (this.function.Calculate(x + this.h) - this.function.Calculate(x)) / this.h;
        }

        private double GetW(double x)
        {
            if (x <= this.u)
            {
                return this.k0;
            }
            return this.k1;
        }

        private bool IsHit(double x)
        {
            return x <= this.u;
        }
    }
}
