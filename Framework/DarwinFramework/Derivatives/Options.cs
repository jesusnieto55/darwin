﻿using CommandLine;

namespace Derivatives
{

    public class Options
    {
        [Option('f', "Function", Required = true, HelpText = "Function to calculate the derivative")]
        public int Function { get; set; }

        [Option('c', "Crossover", Required = false, HelpText = "Crossover Strategy to use", Default = Crossover.NPoint)]
        public Crossover Crossover { get; set; }

        [Option('p', "CrossoverPoints", Required = false, HelpText = "Number of points for NPoint crossover", Default = 2)]
        public int CrossoverPoints { get; set; }

        [Option("CrossoverRate", Required = false, HelpText = "Crossover Rate", Default = 0.3)]
        public double CrossoverRate { get; set; }

        [Option('r', "Runs", Required = true, HelpText = "Number of times the genetic algorithm will be executed")]
        public int Runs { get; set; }

        [Option('n', "Name", Required = false, HelpText = "Name of the test execution")]
        public string Name { get; set; }

        [Option('m', "Mutation", Required = false, HelpText = "Mutation Strategy to use", Default = Mutation.RandomResseting)]
        public Mutation Mutation { get; set; }

        [Option('d', "Duplication", Required = false, HelpText = "Use Duplication", Default = false)]
        public bool UseDuplication { get; set; }

        [Option('l', "LocalSearch", Required = false, HelpText = "Use Local Search", Default = false)]
        public bool UseLocalSearch { get; set; }
    }
}
