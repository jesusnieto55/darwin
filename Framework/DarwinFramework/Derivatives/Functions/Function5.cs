﻿using System;

namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = (e^(2x) + e^(-6x)) / 2
    /// </summary>
    public class Function5 : IFunction
    {
        public int DomainStart => 0;

        public int DomainEnd => 2;

        public double Calculate(double x)
        {
            return (Math.Pow(Math.E, 2*x) + Math.Pow(Math.E, -6*x)) / 2;
        }
    }
}
