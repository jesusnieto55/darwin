﻿using System;

namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = -e^(-2x^2 + 2)
    /// </summary>
    public class Function4 : IFunction
    {
        public int DomainStart => 0;

        public int DomainEnd => 3;

        public double Calculate(double x)
        {
            return -1 * Math.Pow(Math.E, -2*Math.Pow(x,2) + 2);
        }
    }
}
