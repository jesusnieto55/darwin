﻿namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = (x - 2) / (x + 2)
    /// </summary>
    public class Function2 : IFunction
    {
        public int DomainStart => 0;

        public int DomainEnd => 5;

        public double Calculate(double x)
        {
            return (x - 2) / (x + 2);
        }
    }
}
