﻿namespace Derivatives.Functions
{
    public interface IFunction
    {
        int DomainStart { get; }
        int DomainEnd { get; }
        double Calculate(double x);
    }
}
