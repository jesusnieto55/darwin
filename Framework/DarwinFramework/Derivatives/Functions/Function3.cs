﻿namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = 1/5 * (x^2 + 1) * (x - 1)
    /// </summary>
    public class Function3 : IFunction
    {
        public int DomainStart => -2;

        public int DomainEnd => 2;

        public double Calculate(double x)
        {
            double a = (double)1 / (double)5;
            return a * (System.Math.Pow(x, 2) + 1) * (x - 1);
        }
    }
}
