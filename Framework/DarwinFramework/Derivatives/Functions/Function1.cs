﻿namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = x^3 + 8
    /// </summary>
    public class Function1 : IFunction
    {
        public int DomainStart => 0;

        public int DomainEnd => 5;

        public double Calculate(double x)
        {
            return System.Math.Pow(x, 3) + 8;
        }
    }
}
