﻿using System;

namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = x*ln(1+2x)
    /// </summary>
    public class Function6 : IFunction
    {
        public int DomainStart => 0;

        public int DomainEnd => 5;

        public double Calculate(double x)
        {
            return x * Math.Log(1 + 2*x);
        }
    }
}
