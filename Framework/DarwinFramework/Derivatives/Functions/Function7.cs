﻿using System;

namespace Derivatives.Functions
{
    /// <summary>
    /// f(x) = e^(2x) * sin(x)
    /// </summary>
    public class Function7 : IFunction
    {
        public int DomainStart => -2;

        public int DomainEnd => 2;

        public double Calculate(double x)
        {
            return Math.Pow(Math.E, 2*x) * Math.Sin(x);
        }
    }
}
