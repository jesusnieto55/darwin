﻿using CommandLine;
using System;

namespace Derivatives
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Parser.Default.ParseArguments<Options>(args)
                .MapResult(options =>
                {
                    Console.WriteLine("Calculating solution...");

                    var solver = new DerivativeProblemSolver();
                    solver.Solve(options);

                    return 0;
                }, _ => 1);
        }
    }
}
