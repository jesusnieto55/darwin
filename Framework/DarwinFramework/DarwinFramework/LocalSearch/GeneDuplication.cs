﻿using DarwinFramework.Utils;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.LocalSearch
{
    public class GeneDuplication<TGenotype, TGene> : IOffspringImprover<TGenotype>
        where TGenotype : IPermutation<TGene>, ICloneable<TGenotype>
    {
        private readonly RandomGenerator randomGenerator;
        private readonly double duplicationRate;
        private readonly int minCodons;
        private readonly int maxCodons;

        public GeneDuplication(RandomGenerator randomGenerator, double duplicationRate = 0.2, int minCodons = 2, int maxCodons = 10)
        {
            this.randomGenerator = randomGenerator;
            this.duplicationRate = duplicationRate;
            this.minCodons = minCodons;
            this.maxCodons = maxCodons;
        }

        public List<TGenotype> ImproveOffspring(Population<TGenotype> population, List<TGenotype> offspring)
        {
            var improvedOffspring = new List<TGenotype>();

            foreach (var individual in offspring)
            {
                if (this.ShouldImprove())
                {
                    var duplicated = this.Duplicate(individual);
                    improvedOffspring.Add(duplicated);
                }
                else
                {
                    improvedOffspring.Add(individual.Clone());
                }
            }

            return improvedOffspring;
        }

        private TGenotype Duplicate(TGenotype individual)
        {
            var codonsToDuplicate = this.randomGenerator.Generate(this.minCodons, this.maxCodons);
            var codonIndex = this.randomGenerator.Generate(0, individual.Items.Length);

            if (codonIndex + codonsToDuplicate >= individual.Items.Length)
            {
                codonsToDuplicate = individual.Items.Length - codonIndex;
            }

            var codonsDuplicated = individual.Items.ToList().GetRange(codonIndex, codonsToDuplicate);
            var duplicatedIndividual = individual.Clone();
            var duplicatedItems = duplicatedIndividual.Items.ToList();
            duplicatedItems.InsertRange(codonIndex, codonsDuplicated);
            duplicatedIndividual.Items = duplicatedItems.ToArray();

            return duplicatedIndividual;
        }

        private bool ShouldImprove()
        {
            var random = this.randomGenerator.Generate(0, 10);
            return random <= this.duplicationRate * 10;
        }
    }
}
