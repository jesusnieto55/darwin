﻿using System.Collections.Generic;

namespace DarwinFramework.LocalSearch
{
    public interface IOffspringImprover<TGenotype>
    {
        List<TGenotype> ImproveOffspring(Population<TGenotype> population, List<TGenotype> offspring);
    }
}
