﻿using DarwinFramework.Individual;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.ParentSelector
{
    public class EvolutionaryParentSelection<TIndividual> : IParentSelector<TIndividual> where TIndividual : IEvolutionaryIndividual
    {
        public IEnumerable<TIndividual> SelectParents(Population<TIndividual> population, int numberOfParents)
        {
            return population.Individuals.Select(i => i.Individual);
        }
    }
}
