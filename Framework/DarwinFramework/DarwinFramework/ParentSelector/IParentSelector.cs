﻿using System.Collections.Generic;

namespace DarwinFramework.ParentSelector
{
    /// <summary>
    /// Interface that defines the parent selector strategy for a population and a number of parents
    /// </summary>
    /// <typeparam name="TGenotype">The type of the genotype</typeparam>
    public interface IParentSelector<TGenotype>
    {
        IEnumerable<TGenotype> SelectParents(Population<TGenotype> population, int numberOfParents);
    }
}
