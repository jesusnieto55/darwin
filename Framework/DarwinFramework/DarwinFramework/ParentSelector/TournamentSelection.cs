﻿using DarwinFramework.Individual;
using DarwinFramework.Utils;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.ParentSelector
{
    /// <summary>
    /// Implements the parent selection by tournament.
    /// It randomly selects a number of individuals (defined by the tournament size) and selects the best individual using
    /// the fitness calculator. It repeats this process until it selects the number of parents required.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    public class TournamentSelection<TGenotype> : IParentSelector<TGenotype> where TGenotype : ICloneable<TGenotype>
    {
        private readonly TournamentSelectionSettings settings;
        private readonly RandomGenerator randomGenerator;

        public TournamentSelection(TournamentSelectionSettings settings, RandomGenerator randomGenerator)
        {
            this.settings = settings;
            this.randomGenerator = randomGenerator;
        }

        public IEnumerable<TGenotype> SelectParents(Population<TGenotype> population, int numberOfParents)
        {
            var selectedParents = new List<TGenotype>();

            while (selectedParents.Count < numberOfParents)
            {
                var candidates = this.PickRandomnly(population, this.settings.TournamentSize);
                var winner = this.SelectWinner(candidates);

                selectedParents.Add(winner.Clone());
            }

            return selectedParents;
        }

        private TGenotype SelectWinner(List<FitnessIndividual<TGenotype>> candidates)
        {
            return candidates.OrderByDescending(c => c.FitnessValue).FirstOrDefault().Individual.Clone();
        }

        private List<FitnessIndividual<TGenotype>> PickRandomnly(Population<TGenotype> population, int numberOfItems)
        {
            var individualsSelected = new List<FitnessIndividual<TGenotype>>();
            var indexesSelected = new List<int>();

            while (individualsSelected.Count < numberOfItems
                && individualsSelected.Count < population.Individuals.Count())
            {
                var randomIndex = this.randomGenerator.Generate(0, population.Individuals.Count() - 1);

                if (!indexesSelected.Contains(randomIndex))
                {
                    var individual = population.Individuals.ToList()[randomIndex];
                    individualsSelected.Add(new FitnessIndividual<TGenotype>
                    {
                        Individual = individual.Individual.Clone(),
                        FitnessValue = individual.FitnessValue
                    });
                    indexesSelected.Add(randomIndex);
                }
            }

            return individualsSelected;
        }
    }
}
