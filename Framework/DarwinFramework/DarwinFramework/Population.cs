﻿using DarwinFramework.Individual;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework
{
    /// <summary>
    /// Represents a population in a genetic algorithm.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    public class Population<TGenotype>
    {
        private readonly FitnessCalculator<TGenotype> fitnessCalculator;

        public IEnumerable<FitnessIndividual<TGenotype>> Individuals { get; private set; }
        public IEnumerable<FitnessIndividual<TGenotype>> IndividualsOrderedByFitness { get; private set; }
        public double AverageFitness { get; private set; }
        public int Size => this.Individuals.Count();

        public Population(FitnessCalculator<TGenotype> fitnessCalculator)
        {
            this.fitnessCalculator = fitnessCalculator;
        }

        public void SetIndividuals(IEnumerable<TGenotype> Individuals)
        {
            this.Individuals = Individuals.Select(i => new FitnessIndividual<TGenotype>
            {
                Individual = i,
                FitnessValue = this.fitnessCalculator.CalculateFitness(i)
            }).ToList();
            this.IndividualsOrderedByFitness = this.Individuals.OrderByDescending(i => i.FitnessValue).ToList();
            this.AverageFitness = this.IndividualsOrderedByFitness.Sum(i => i.FitnessValue) / (double)this.IndividualsOrderedByFitness.Count();
        }

        public FitnessIndividual<TGenotype> GetBestIndividual()
            => this.IndividualsOrderedByFitness.FirstOrDefault();

        public FitnessIndividual<TGenotype> GetWorstIndividual()
            => this.IndividualsOrderedByFitness.LastOrDefault();
    }
}
