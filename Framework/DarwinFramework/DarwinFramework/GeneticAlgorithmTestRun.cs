﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace DarwinFramework
{
    public class GeneticAlgorithmTestRun<TGenotype> where TGenotype : ICloneable<TGenotype>
    {
        private readonly GeneticAlgorithm<TGenotype> geneticAlgorithm;
        private readonly int executions;

        public GeneticAlgorithmTestRun(GeneticAlgorithm<TGenotype> geneticAlgorithm, int executions)
        {
            this.geneticAlgorithm = geneticAlgorithm;
            this.executions = executions;
        }

        public void Execute(IEnumerable<TGenotype> firstGeneration, string name)
        {
            var results = new List<GeneticAlgorithmResult<TGenotype>>();

            for (int i = 0; i < executions; i++)
            {
                results.Add(this.geneticAlgorithm.Start(firstGeneration));
            }

            this.SaveResult(results, name);
        }

        private void SaveResult(List<GeneticAlgorithmResult<TGenotype>> results, string name)
        {
            var lines = new List<string>();
            var header = "Fitness,AES,Time,Generations,Result";
            lines.Add("----------------Runs-------------------");
            lines.Add(header);

            var culture = CultureInfo.CreateSpecificCulture("en-US");
            lines.AddRange(
                results.Select(r => $"{r.BestIndividual.FitnessValue.ToString(culture)},{r.FitnessEvaluations},{r.Time},{r.Generations},{r.BestIndividual.Individual}"));

            var averageFitness = results.Sum(r => r.BestIndividual.FitnessValue) / results.Count;
            var averageEvaluations = results.Sum(r => r.FitnessEvaluations) / results.Count;
            var averageTime = results.Sum(r => r.Time) / results.Count;
            var averageGenerations = results.Sum(r => r.Generations) / results.Count;

            lines.Add("----------------Average----------------");
            lines.Add(header);
            lines.Add(string.Join(',', averageFitness, averageEvaluations, averageTime, averageGenerations));

            var outputFolder = "output";
            Directory.CreateDirectory(outputFolder);
            var date = DateTime.Now.ToString("yyyyMMddHHmmss");

            System.IO.File.WriteAllLines($@".\{outputFolder}\{name}_{date}.csv", lines);
        }
    }
}
