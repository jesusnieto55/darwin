﻿namespace DarwinFramework.Individual
{
    public interface ILifetimeIndividual
    {
        int? RemainingLifetime { get; set; }
    }
}
