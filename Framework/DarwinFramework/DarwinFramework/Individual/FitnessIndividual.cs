﻿namespace DarwinFramework.Individual
{
    public class FitnessIndividual<T>
    {
        public T Individual { get; set; }
        public double FitnessValue { get; set; }
    }
}
