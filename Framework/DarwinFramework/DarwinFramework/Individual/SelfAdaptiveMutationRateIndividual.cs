﻿using System;
using System.Linq;

namespace DarwinFramework.Individual
{
    public abstract class SelfAdaptiveMutationRateIndividual
    {
        public bool[] EncodedMutationRate { get; set; }

        public double DecodeMutationRate()
        {
            var integerRepresentation = this.GetInteger();
            return integerRepresentation / ((Math.Pow(2, EncodedMutationRate.Count()) - 1));
        }

        private int GetInteger()
        {
            var reversedBits = this.EncodedMutationRate.Reverse().ToArray();
            var num = 0;
            for (var power = 0; power < reversedBits.Count(); power++)
            {
                var currentBit = reversedBits[power];
                if (currentBit)
                {
                    var currentNum = (int)Math.Pow(2, power);
                    num += currentNum;
                }
            }

            return num;
        }
    }
}
