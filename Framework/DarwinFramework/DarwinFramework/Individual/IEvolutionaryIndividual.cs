﻿namespace DarwinFramework.Individual
{
    public interface IEvolutionaryIndividual
    {
        double[] Genotype { get; set; }
        double[] StrategyParameters { get; set; }
    }
}
