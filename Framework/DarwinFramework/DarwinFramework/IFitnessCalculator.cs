﻿namespace DarwinFramework
{
    public abstract class FitnessCalculator<TGenotype>
    {
        public int NumberOfEvaluations { get; private set; }

        public virtual double CalculateFitness(TGenotype individual)
        {
            this.NumberOfEvaluations++;
            return this.GetFitness(individual);
        }

        public abstract double GetFitness(TGenotype individual);
    }
}
