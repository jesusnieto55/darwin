﻿using DarwinFramework.Individual;

namespace DarwinFramework
{
    public class GeneticAlgorithmResult<TGenotype>
    {
        public FitnessIndividual<TGenotype> BestIndividual { get; private set; }
        public int Generations { get; private set; }
        public long Time { get; private set; }
        public int FitnessEvaluations { get; private set; }

        public GeneticAlgorithmResult(FitnessIndividual<TGenotype> bestIndividual, int generations, long time, int fitnessEvaluations)
        {
            this.BestIndividual = bestIndividual;
            this.Generations = generations;
            this.Time = time;
            this.FitnessEvaluations = fitnessEvaluations;
        }
    }
}
