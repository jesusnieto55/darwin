﻿namespace DarwinFramework
{
    /// <summary>
    /// Interface that specifies an individual is represented by a permutation of items
    /// </summary>
    /// <typeparam name="TGenotype"></typeparam>
    public interface IPermutation<TGenotype>
    {
        TGenotype[] Items { get; set; }
    }
}
