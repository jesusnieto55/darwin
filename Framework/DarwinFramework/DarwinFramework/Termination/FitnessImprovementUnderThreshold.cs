﻿namespace DarwinFramework.Termination
{
    /// <summary>
    /// Implements a termination strategy that terminates if during a certain number of generations, the fitness improvement
    /// is lower than a threshold
    /// </summary>
    /// <typeparam name="TGenotype"></typeparam>
    public class FitnessImprovementUnderThreshold<TGenotype> : ITerminationStrategy<TGenotype>
    {
        private readonly FitnessImprovementUnderThresholdSettings settings;
        private int generationsWithoutImprovement = 0;
        private double lastBestFitness = 0;

        public FitnessImprovementUnderThreshold(FitnessImprovementUnderThresholdSettings settings)
        {
            this.settings = settings;
        }

        public bool Terminate(Population<TGenotype> population, int numberOfGenerations)
        {
            this.CheckImprovement(population);

            return this.generationsWithoutImprovement >= this.settings.NumberOfGenerations;
        }

        private void CheckImprovement(Population<TGenotype> population)
        {
            var bestFitness = population.GetBestIndividual().FitnessValue;

            if (bestFitness - this.lastBestFitness < this.settings.Threshold)
            {
                this.generationsWithoutImprovement++;
            }
            else
            {
                this.generationsWithoutImprovement = 0;
            }
            this.lastBestFitness = bestFitness;
        }
    }
}
