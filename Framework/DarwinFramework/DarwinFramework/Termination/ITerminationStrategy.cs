﻿namespace DarwinFramework.Termination
{
    /// <summary>
    /// Interface that defines the termination strategy for a population
    /// </summary>
    /// <typeparam name="TGenotype"></typeparam>
    public interface ITerminationStrategy<TGenotype>
    {
        bool Terminate(Population<TGenotype> population, int numberOfGenerations);
    }
}
