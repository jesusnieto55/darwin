﻿namespace DarwinFramework.Termination
{
    public class FixedGenerationsTerminationStrategy<TGenotype> : ITerminationStrategy<TGenotype>
    {
        private readonly int maxGenerations;

        public FixedGenerationsTerminationStrategy(int maxGenerations)
        {
            this.maxGenerations = maxGenerations;
        }

        public bool Terminate(Population<TGenotype> population, int numberOfGenerations)
        {
            return numberOfGenerations >= this.maxGenerations;
        }
    }
}
