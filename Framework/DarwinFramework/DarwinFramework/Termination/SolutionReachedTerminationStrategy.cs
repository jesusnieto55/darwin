﻿using DarwinFramework;
using System;

namespace DarwinFramework.Termination
{
    public class SolutionReachedTerminationStrategy<TGenotype> : ITerminationStrategy<TGenotype>
    {
        private readonly int maxGenerations;
        private readonly double fitnessGoal;
        private readonly double errorMargin;

        public SolutionReachedTerminationStrategy(int maxGenerations, double fitnessGoal, double errorMargin)
        {
            this.maxGenerations = maxGenerations;
            this.fitnessGoal = fitnessGoal;
            this.errorMargin = errorMargin;
        }

        public bool Terminate(Population<TGenotype> population, int numberOfGenerations)
        {
            return numberOfGenerations == maxGenerations || Math.Abs(population.GetBestIndividual().FitnessValue) <= Math.Abs(fitnessGoal - errorMargin);
        }
    }
}
