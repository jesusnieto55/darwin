﻿namespace DarwinFramework.Termination
{
    public class FitnessImprovementUnderThresholdSettings
    {
        public int NumberOfGenerations { get; set; }
        public double Threshold { get; set; }
    }
}
