﻿using System;

namespace DarwinFramework.Utils
{
    public class RandomGenerator
    {
        /// <summary>
        /// Generates a new random integer that is between min and max (both inclusive)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public virtual int Generate(int min, int max)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            return random.Next(min, max + 1);
        }

        /// <summary>
        /// Generates a new random double that is between 0 (inclusive) and 1 (exclusive)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public virtual double GenerateDouble()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            return random.NextDouble();
        }
    }
}
