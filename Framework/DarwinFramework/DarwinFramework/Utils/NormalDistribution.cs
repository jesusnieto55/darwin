﻿using System;

namespace DarwinFramework.Utils
{
    public class NormalDistribution
    {
        private readonly RandomGenerator randomGenerator;

        public NormalDistribution(RandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        // TODO: Need to check if this is correct
        public double Generate(double mean, double stadardDeviation)
        {
            double u1 = 1.0 - this.randomGenerator.GenerateDouble();
            double u2 = 1.0 - this.randomGenerator.GenerateDouble();

            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
            double randNormal = mean + stadardDeviation * randStdNormal;

            return randNormal;
        }
    }
}
