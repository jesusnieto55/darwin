﻿using System.Collections.Generic;
using System.Linq;
using DarwinFramework.Mutation;
using DarwinFramework.Recombination;
using DarwinFramework.ParentSelector;
using DarwinFramework.SurvivorSelector;
using DarwinFramework.Termination;
using DarwinFramework.Statistics;
using DarwinFramework.Utils;
using System;
using DarwinFramework.OffspringGeneration;
using System.Diagnostics;
using DarwinFramework.LocalSearch;

namespace DarwinFramework
{
    /// <summary>
    /// Class that contains the flow of a genetic algorithm.
    /// It performs the following steps:
    ///     1. Initialize Population
    ///     2. Select parents
    ///     3. Generate offspring
    ///     4. Mutate
    ///     5. Select survivors
    /// </summary>
    /// <typeparam name="TGenotype"></typeparam>
    public class GeneticAlgorithm<TGenotype> where TGenotype : ICloneable<TGenotype>
    {
        private IMutationStrategy<TGenotype> mutationStrategy;
        private IOffspringGenerationStrategy<TGenotype> offspringGenerationStrategy;
        private ITerminationStrategy<TGenotype> terminationStrategy;
        private ISurvivorSelector<TGenotype> survivorSelector;
        private IParentSelector<TGenotype> parentSelector;
        private readonly FitnessCalculator<TGenotype> fitnessCalculator;
        private readonly Settings settings;
        private readonly IOffspringImprover<TGenotype> offspringImprover;
        private Stopwatch stopwatch;

        public Population<TGenotype> Population { get; private set; }
        public History<TGenotype> History { get; set; }
        public int NumberOfGenerations { get; private set; }

        public GeneticAlgorithm(
            IMutationStrategy<TGenotype> mutationStrategy,
            IOffspringGenerationStrategy<TGenotype> offspringGenerationStrategy,
            ITerminationStrategy<TGenotype> terminationStrategy,
            ISurvivorSelector<TGenotype> survivorSelector,
            IParentSelector<TGenotype> parentSelector,
            FitnessCalculator<TGenotype> fitnessCalculator,
            Settings settings,
            IOffspringImprover<TGenotype> offspringImprover = null)
        {
            this.mutationStrategy = mutationStrategy;
            this.offspringGenerationStrategy = offspringGenerationStrategy;
            this.terminationStrategy = terminationStrategy;
            this.survivorSelector = survivorSelector;
            this.parentSelector = parentSelector;
            this.fitnessCalculator = fitnessCalculator;
            this.settings = settings;
            this.offspringImprover = offspringImprover;
            this.Population = new Population<TGenotype>(this.fitnessCalculator);
            this.History = new History<TGenotype>();
            this.NumberOfGenerations = 0;
        }

        public GeneticAlgorithmResult<TGenotype> Start(IEnumerable<TGenotype> firstGeneration)
        {
            // 1. Initialize Population
            // 2. Select parents
            // 3. Generate offspring
            // 4. Mutate
            // 5. Select survivors
            this.StartWatch();
            this.NumberOfGenerations = 0;
            this.Population.SetIndividuals(firstGeneration);

            do
            {
                var parents = this.parentSelector.SelectParents(this.Population, this.settings.NumberOfParents).ToList();
                var offspring = this.offspringGenerationStrategy.GenerateOffspring(this.Population, parents, this.NumberOfGenerations);
                var mutatedOffspring = offspring.Select(child => this.mutationStrategy.Mutate(child)).ToList();
                if (this.offspringImprover != null)
                {
                    mutatedOffspring = this.offspringImprover.ImproveOffspring(this.Population, mutatedOffspring);
                }
                var survivors = this.survivorSelector.SelectSurvivors(this.Population, mutatedOffspring).ToList();

                this.Population.SetIndividuals(survivors);
                this.NumberOfGenerations++;
                this.AddStatistics();

            } while (!this.terminationStrategy.Terminate(this.Population, this.NumberOfGenerations));

            this.StopWatch();
            this.SaveHistory();

            return new GeneticAlgorithmResult<TGenotype>(
                this.Population.GetBestIndividual(),
                this.NumberOfGenerations,
                this.stopwatch.ElapsedMilliseconds,
                this.fitnessCalculator.NumberOfEvaluations);
        }

        private void AddStatistics()
        {
            var bestIndividual = this.Population.GetBestIndividual();
            var averageFitness = this.Population.AverageFitness;
            var populationSize = this.Population.Size;
            this.History.RegisterGeneration(this.NumberOfGenerations, bestIndividual);

            Console.WriteLine($"Generation: {this.NumberOfGenerations} -- Population: {populationSize} -- Average Fitness: {averageFitness} -- Best Fitness: {bestIndividual?.FitnessValue}");
        }

        private void StartWatch()
        {
            this.stopwatch = Stopwatch.StartNew();
        }

        private void StopWatch()
        {
            this.stopwatch.Stop();
        }

        private void SaveHistory()
        {
            this.History.ExecutionTime = this.stopwatch.ElapsedMilliseconds;
            this.History.NumberOfEvaluations = this.fitnessCalculator.NumberOfEvaluations;

            if (this.settings.SaveHistory)
            {
                this.History.Save();
            }
        }
    }
}