﻿using DarwinFramework.Individual;
using System.Collections.Generic;

namespace DarwinFramework.Statistics
{
    public class History<TGenotype>
    {
        private readonly HistorySaver<TGenotype> historySaver;
        public List<Generation<TGenotype>> Generations { get; private set; }
        public int NumberOfEvaluations { get; set; }
        public long ExecutionTime { get; set; }

        public History()
        {
            this.Generations = new List<Generation<TGenotype>>();
            this.historySaver = new HistorySaver<TGenotype>();
        }

        public void RegisterGeneration(int generationNumber, FitnessIndividual<TGenotype> bestIndividual)
        {
            this.Generations.Add(new Generation<TGenotype>()
            {
                BestIndividual = bestIndividual,
                GenerationNumber = generationNumber
            });
        }

        public void Save()
        {
            this.historySaver.Save(this);
        }
    }
}
