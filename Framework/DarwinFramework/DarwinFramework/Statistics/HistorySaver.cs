﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;
using System.IO;

namespace DarwinFramework.Statistics
{
    public class HistorySaver<TGenotype>
    {
        private readonly string outputFolder = @".\output";

        public void Save(History<TGenotype> history)
        {
            var lines = new List<string>();// { "Generation, BestFitness" };

            var culture = CultureInfo.CreateSpecificCulture("en-US");
            lines.AddRange(
                history.Generations.Select(g => $"{g.GenerationNumber}, {g.BestIndividual.FitnessValue.ToString(culture)}"));

            var lastBestIndividual = history.Generations.LastOrDefault()?.BestIndividual;
            lines.Add($"Final Solution ({lastBestIndividual.FitnessValue}) : {lastBestIndividual.Individual.ToString()}");
            lines.Add($"Summary --> Best Fitness: {Round(lastBestIndividual.FitnessValue)} \tAES: {history.NumberOfEvaluations} \tExecution Time: {history.ExecutionTime}");

            Directory.CreateDirectory(outputFolder);
            var date = DateTime.Now.ToString("yyyyMMddHHmmss");

            System.IO.File.WriteAllLines($@".\{outputFolder}\{date}.csv", lines);
        }

        private string Round(double value) => Math.Round(value, 4).ToString();
    }
}
