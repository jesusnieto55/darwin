﻿using DarwinFramework.Individual;

namespace DarwinFramework.Statistics
{
    public class Generation<TGenotype>
    {
        public int GenerationNumber { get; set; }
        public FitnessIndividual<TGenotype> BestIndividual { get; set; }
    }
}
