﻿using DarwinFramework.Individual;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.SurvivorSelector
{
    /// <summary>
    /// Implements the lifetimee based survivor selector strategy in which every individual has a remaining lifetime that gets
    /// decreased after every generation. Those individuals with remaining lifetime 0 are removed from the population.
    /// We make an exception for the fittest individual, whose remaining lifetime does not get decreased.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    public class LifetimeBasedSurvivorSelector<TGenotype> : ISurvivorSelector<TGenotype> where TGenotype : ILifetimeIndividual
    {
        private readonly FitnessCalculator<TGenotype> fitnessCalculator;
        private readonly int maxLifetime;
        private readonly int minLifetime;

        public LifetimeBasedSurvivorSelector(FitnessCalculator<TGenotype> fitnessCalculator, int maxLifetime = 11, int minLifetime = 0)
        {
            this.fitnessCalculator = fitnessCalculator;
            this.maxLifetime = maxLifetime;
            this.minLifetime = minLifetime;
        }

        public IEnumerable<TGenotype> SelectSurvivors(Population<TGenotype> population, IEnumerable<TGenotype> offspring)
        {
            var newGenerationPopulation = new Population<TGenotype>(this.fitnessCalculator);

            var newGenerationIndividuals = population.Individuals.Select(i => i.Individual).ToList();
            newGenerationIndividuals.AddRange(offspring);
            newGenerationPopulation.SetIndividuals(newGenerationIndividuals);

            var averageFitness = newGenerationPopulation.AverageFitness;
            var bestFitness = newGenerationPopulation.GetBestIndividual().FitnessValue;
            var worstFitness = newGenerationPopulation.GetWorstIndividual().FitnessValue;

            foreach (var fitnessIndividual in newGenerationPopulation.Individuals)
            {
                if (fitnessIndividual.Individual.RemainingLifetime == null)
                {
                    fitnessIndividual.Individual.RemainingLifetime =
                        this.CalculateRemainingLifetime(fitnessIndividual.Individual, averageFitness, bestFitness, worstFitness);
                }
            }

            this.DecreaseRemainingLifetime(newGenerationPopulation);

            return newGenerationPopulation.IndividualsOrderedByFitness
                .Where(i => i.Individual.RemainingLifetime > 0)
                .Select(i => i.Individual).ToList();
        }

        private void DecreaseRemainingLifetime(Population<TGenotype> population)
        {
            var bestIndividual = population.GetBestIndividual();

            foreach (var fitnessIndividual in population.Individuals)
            {
                if (fitnessIndividual != bestIndividual)
                {
                    fitnessIndividual.Individual.RemainingLifetime--;
                }
            }
        }

        private int CalculateRemainingLifetime(TGenotype individual, double averageFitness, double bestFitness, double worstFitness)
        {
            var fitness = this.fitnessCalculator.CalculateFitness(individual);
            var n = 0.5 * (this.maxLifetime - this.minLifetime);

            if (fitness <= averageFitness)
            {
                return (int)(this.minLifetime * n * ((worstFitness - fitness) / (worstFitness - averageFitness)));
            }
            return (int)(0.5 * (this.minLifetime + this.maxLifetime) + n * ((averageFitness - fitness) / (averageFitness - bestFitness)));
        }
    }
}
