﻿using DarwinFramework.Individual;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.SurvivorSelector
{
    /// <summary>
    /// Implements the age based survivor selector strategy in which the whole population is replaced by the offspring.
    /// It applies elitism, so if the best of the parents is better than the best offspring, this parent will survive
    /// and be passed to the next generation (and the worst offspring will be removed)
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    public class AgeBasedSurvivorSelector<TGenotype> : ISurvivorSelector<TGenotype>
    {
        private readonly FitnessCalculator<TGenotype> fitnessCalculator;

        public AgeBasedSurvivorSelector(FitnessCalculator<TGenotype> fitnessCalculator)
        {
            this.fitnessCalculator = fitnessCalculator;
        }

        public IEnumerable<TGenotype> SelectSurvivors(Population<TGenotype> population, IEnumerable<TGenotype> offspring)
        {
            var populationByFitness = population.IndividualsOrderedByFitness;
            var offspringByFitness = this.IndividualsOrderedByFitness(offspring);
            var bestOffspring = offspringByFitness.FirstOrDefault();
            var worstOffspring = offspringByFitness.LastOrDefault();
            var bestInPopulation = populationByFitness.FirstOrDefault();

            var newGeneration = offspring.ToList();

            if (bestInPopulation.FitnessValue > bestOffspring?.FitnessValue)
            {
                newGeneration.Remove(worstOffspring.Individual);
                newGeneration.Add(bestInPopulation.Individual);
            }

            return newGeneration;
        }

        private IEnumerable<FitnessIndividual<TGenotype>> IndividualsOrderedByFitness(IEnumerable<TGenotype> individuals)
        {
            return individuals.Select(c => new FitnessIndividual<TGenotype>
            {
                Individual = c,
                FitnessValue = fitnessCalculator.CalculateFitness(c)
            }).OrderByDescending(c => c.FitnessValue).ToList();
        }
    }
}
