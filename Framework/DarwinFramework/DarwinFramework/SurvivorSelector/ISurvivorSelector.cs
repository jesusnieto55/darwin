﻿using System.Collections.Generic;

namespace DarwinFramework.SurvivorSelector
{
    /// <summary>
    /// Interface that defines the survivor selector strategy for a population of individuals and a set of offsprings.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    public interface ISurvivorSelector<TGenotype>
    {
        IEnumerable<TGenotype> SelectSurvivors(Population<TGenotype> population, IEnumerable<TGenotype> offspring);
    }
}
