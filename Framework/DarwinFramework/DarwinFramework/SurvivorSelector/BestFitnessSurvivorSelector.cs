﻿using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.SurvivorSelector
{
    public class BestFitnessSurvivorSelector<TGenotype> : ISurvivorSelector<TGenotype> where TGenotype : ICloneable<TGenotype>
    {
        private readonly FitnessCalculator<TGenotype> fitnessCalculator;
        private readonly bool onlyOffspring;

        public BestFitnessSurvivorSelector(FitnessCalculator<TGenotype> fitnessCalculator, bool onlyOffspring)
        {
            this.fitnessCalculator = fitnessCalculator;
            this.onlyOffspring = onlyOffspring;
        }

        public IEnumerable<TGenotype> SelectSurvivors(Population<TGenotype> population, IEnumerable<TGenotype> offspring)
        {
            var individuals = offspring.Select(o => o.Clone()).ToList();
            if (!this.onlyOffspring)
            {
                individuals.AddRange(population.Individuals.Select(i => i.Individual));
            }

            return individuals.OrderByDescending(c => fitnessCalculator.CalculateFitness(c)).Take(population.Individuals.Count());
        }
    }
}
