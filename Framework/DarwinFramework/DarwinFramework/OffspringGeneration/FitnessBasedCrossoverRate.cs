﻿namespace DarwinFramework.OffspringGeneration
{
    public class FitnessBasedCrossoverRate<TGenotype> : ICrossoverRateStrategy<TGenotype>
    {
        private readonly FitnessCalculator<TGenotype> fitnessCalculator;

        public FitnessBasedCrossoverRate(FitnessCalculator<TGenotype> fitnessCalculator)
        {
            this.fitnessCalculator = fitnessCalculator;
        }

        public double GetCrossoverRate(Population<TGenotype> population, TGenotype parent1, TGenotype parent2, int generation)
        {
            var fitness1 = this.fitnessCalculator.CalculateFitness(parent1);
            var fitness2 = this.fitnessCalculator.CalculateFitness(parent2);

            var crossoverRate1 = this.GetCrossoverRateByFitness(population, fitness1);
            var crossoverRate2 = this.GetCrossoverRateByFitness(population, fitness2);

            var crossoverRate = (crossoverRate1 + crossoverRate2) / 2;
            // System.Console.WriteLine($"CrossoverRate: {crossoverRate}");
            return crossoverRate;
        }

        private double GetCrossoverRateByFitness(Population<TGenotype> population, double fitness)
        {
            var minFitness = population.GetWorstIndividual().FitnessValue;
            var maxFitness = population.GetBestIndividual().FitnessValue;

            return (fitness * (maxFitness - 1) / (-minFitness)) - (1 / (-minFitness));
        }
    }
}
