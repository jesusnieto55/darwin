﻿using DarwinFramework.Individual;
using DarwinFramework.Utils;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.OffspringGeneration
{
    public class GlobalEvolutionaryOffspringStrategy<TIndividual> : IOffspringGenerationStrategy<TIndividual> where TIndividual : IEvolutionaryIndividual, new()
    {
        private readonly RandomGenerator randomGenerator;
        private readonly int numberOfChildren;

        public GlobalEvolutionaryOffspringStrategy(RandomGenerator randomGenerator, int numberOfChildren)
        {
            this.randomGenerator = randomGenerator;
            this.numberOfChildren = numberOfChildren;
        }

        public IEnumerable<TIndividual> GenerateOffspring(Population<TIndividual> population, List<TIndividual> parents, int generation)
        {
            var offspring = new List<TIndividual>();

            for (int i = 0; i < this.numberOfChildren; i++)
            {
                var genotype = this.GenerateGenotype(parents);
                var strategyParameters = this.GenerateStrategyParameters(parents);

                offspring.Add(new TIndividual { Genotype = genotype, StrategyParameters = strategyParameters });
            }

            return offspring;
        }

        private double[] GenerateGenotype(List<TIndividual> parents)
        {
            var genotypeLength = parents.FirstOrDefault().Genotype.Count();
            var genotype = new List<double>();

            for (int i = 0; i < genotypeLength; i++)
            {
                var random1 = this.randomGenerator.Generate(0, parents.Count - 1);
                var random2 = this.randomGenerator.Generate(0, parents.Count - 1);

                var parent1 = parents[random1];
                var parent2 = parents[random2];

                var random = this.randomGenerator.Generate(0, 1);
                if (random == 0)
                {
                    genotype.Add(parent1.Genotype[i]);
                }
                else
                {
                    genotype.Add(parent2.Genotype[i]);
                }
            }
            return genotype.ToArray();
        }

        private double[] GenerateStrategyParameters(List<TIndividual> parents)
        {
            var strategyParameters = new List<double>();
            var strategyLenght = parents.FirstOrDefault().StrategyParameters.Count();

            for (int i = 0; i < strategyLenght; i++)
            {
                var random1 = this.randomGenerator.Generate(0, parents.Count - 1);
                var random2 = this.randomGenerator.Generate(0, parents.Count - 1);

                var parent1 = parents[random1];
                var parent2 = parents[random2];

                var strategyParameter = (parent1.StrategyParameters[i] + parent2.StrategyParameters[i]) / 2;
                strategyParameters.Add(strategyParameter);
            }
            return strategyParameters.ToArray();
        }
    }
}
