﻿using DarwinFramework.Recombination;
using DarwinFramework.Utils;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DarwinFramework.OffspringGeneration
{
    public class SexualOffspringGenerationStrategy<TGenotype> : IOffspringGenerationStrategy<TGenotype> where TGenotype : ICloneable<TGenotype>
    {
        private readonly RandomGenerator randomGenerator;
        private readonly IRecombinationStrategy<TGenotype> recombinationStrategy;
        private readonly ICrossoverRateStrategy<TGenotype> crossOverRateStrategy;

        public SexualOffspringGenerationStrategy(RandomGenerator randomGenerator, IRecombinationStrategy<TGenotype> recombinationStrategy, ICrossoverRateStrategy<TGenotype> crossOverRateStrategy)
        {
            this.randomGenerator = randomGenerator;
            this.recombinationStrategy = recombinationStrategy;
            this.crossOverRateStrategy = crossOverRateStrategy;
        }

        public IEnumerable<TGenotype> GenerateOffspring(Population<TGenotype> population, List<TGenotype> parents, int generation)
        {
            var offspring = new ConcurrentBag<TGenotype>();
            var parentGroups = new List<List<TGenotype>>();

            for (var i = 0; i < parents.Count(); i += 2)
            {
                var parent1 = parents[i];
                bool secondParentExists = i + 1 < parents.Count;

                if (secondParentExists)
                {
                    var parent2 = parents[i + 1];

                    parentGroups.Add(new List<TGenotype> { parent1, parent2 });
                }
            }

            foreach (var couple in parentGroups)
            {
                var parent1 = couple[0];
                var parent2 = couple[1];

                if (this.ShouldRecombine(population, parent1, parent2, generation))
                {
                    var children = this.recombinationStrategy.Recombine(parent1, parent2).ToList();

                    offspring.Add(children[0]);
                    offspring.Add(children[1]);
                }
                else
                {
                    var cloned1 = parent1.Clone();
                    var cloned2 = parent2.Clone();
                    offspring.Add(cloned1);
                    offspring.Add(cloned2);
                }
            }

            //Parallel.ForEach(parentGroups, (couple) =>
            //{
            //    var parent1 = couple[0];
            //    var parent2 = couple[1];

            //    if (this.ShouldRecombine(parent1, parent2, generation))
            //    {
            //        var children = this.recombinationStrategy.Recombine(parent1, parent2).ToList();

            //        offspring.Add(children[0]);
            //        offspring.Add(children[1]);
            //    }
            //    else
            //    {
            //        var cloned1 = parent1.Clone();
            //        var cloned2 = parent2.Clone();
            //        offspring.Add(cloned1);
            //        offspring.Add(cloned2);
            //    }
            //});

            return offspring;
        }

        private bool ShouldRecombine(Population<TGenotype> population, TGenotype parent1, TGenotype parent2, int generation)
        {
            var random = this.randomGenerator.Generate(0, 1000);
            return random <= (this.crossOverRateStrategy.GetCrossoverRate(population, parent1, parent2, generation) * 1000);
        }
    }
}
