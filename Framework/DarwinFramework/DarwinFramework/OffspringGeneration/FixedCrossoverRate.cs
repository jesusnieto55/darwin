﻿namespace DarwinFramework.OffspringGeneration
{
    public class FixedCrossoverRate<TGenotype> : ICrossoverRateStrategy<TGenotype>
    {
        private readonly double crossoverRate;

        public FixedCrossoverRate(double crossoverRate)
        {
            this.crossoverRate = crossoverRate;
        }

        public double GetCrossoverRate(Population<TGenotype> population, TGenotype parent1, TGenotype parent2, int generation) => crossoverRate;
    }
}
