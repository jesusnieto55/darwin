﻿using System.Collections.Generic;

namespace DarwinFramework.OffspringGeneration
{
    public interface IOffspringGenerationStrategy<TGenotype>
    {
        IEnumerable<TGenotype> GenerateOffspring(Population<TGenotype> population, List<TGenotype> parents, int generation);
    }
}
