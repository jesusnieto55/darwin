﻿namespace DarwinFramework.OffspringGeneration
{
    public class GenerationBasedCrossoverRate<TGenotype> : ICrossoverRateStrategy<TGenotype>
    {
        private readonly int maxGenerations;

        public GenerationBasedCrossoverRate(int maxGenerations)
        {
            this.maxGenerations = maxGenerations;
        }

        public double GetCrossoverRate(Population<TGenotype> population, TGenotype parent1, TGenotype parent2, int generation)
        {
            var crossoverRate = ((double)-generation / (double)maxGenerations) + 1;
            return crossoverRate;
        }
    }
}
