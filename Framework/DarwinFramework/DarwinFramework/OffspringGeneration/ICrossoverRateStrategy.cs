﻿namespace DarwinFramework.OffspringGeneration
{
    public interface ICrossoverRateStrategy<TGenotype>
    {
        double GetCrossoverRate(Population<TGenotype> population, TGenotype parent1, TGenotype parent2, int generation);
    }
}
