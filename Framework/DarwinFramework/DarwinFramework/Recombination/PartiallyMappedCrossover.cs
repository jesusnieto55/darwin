﻿using DarwinFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.Recombination
{
    /// <summary>
    /// Implements the partially mapped crossover (MPX) recombination strategy for two individuals.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    /// <typeparam name="TGene">Type of the gene</typeparam>
    public class PartiallyMappedCrossover<TGenotype, TGene> : IRecombinationStrategy<TGenotype>
        where TGenotype : IPermutation<TGene>, ICloneable<TGenotype>, new()
    {
        private readonly RandomGenerator randomGenerator;

        public PartiallyMappedCrossover() : this(new RandomGenerator()) { }

        public PartiallyMappedCrossover(RandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public IEnumerable<TGenotype> Recombine(TGenotype parent1, TGenotype parent2)
        {
            var random1 = this.randomGenerator.Generate(0, parent1.Items.Count() - 1);
            var random2 = this.randomGenerator.Generate(0, parent2.Items.Count() - 1);

            var crossoverPoint1 = Math.Min(random1, random2);
            var crossoverPoint2 = Math.Max(random1, random2);

            var offspring1 = this.GenerateOffspring(parent1, parent2, crossoverPoint1, crossoverPoint2);
            var offspring2 = this.GenerateOffspring(parent2, parent1, crossoverPoint1, crossoverPoint2);

            return new[] { offspring1, offspring2 };
        }

        private TGenotype GenerateOffspring(
            TGenotype parent1Genotype,
            TGenotype parent2Genotype,
            int crossoverPoint1,
            int crossoverPoint2)
        {
            var parent1Cloned = parent1Genotype.Clone();
            var parent2Cloned = parent2Genotype.Clone();

            var offspringGenotype = parent1Cloned.Items.Select((item, i) => new ItemAssigned { Item = item, Assigned = (i >= crossoverPoint1 && i <= crossoverPoint2) }).ToList();

            // Mappped crossover
            for (var i = crossoverPoint1; i <= crossoverPoint2; i++)
            {
                var itemInParent1 = parent1Cloned.Items[i];
                var itemInParent2 = parent2Cloned.Items[i];
                var indexItemP1InP2 = parent2Cloned.Items.ToList().IndexOf(itemInParent1);

                if (offspringGenotype.FirstOrDefault(item => item.Item.Equals(itemInParent2))?.Assigned == true)
                {
                    continue;
                }

                while(offspringGenotype[indexItemP1InP2].Assigned)
                {
                    var itemInOffspring = offspringGenotype[indexItemP1InP2].Item;
                    indexItemP1InP2 = parent2Cloned.Items.ToList().IndexOf(itemInOffspring);
                }

                offspringGenotype[indexItemP1InP2].Item = itemInParent2;
                offspringGenotype[indexItemP1InP2].Assigned = true;
            }

            // Fill remaining items
            for (var i = 0; i < offspringGenotype.Count(); i++)
            {
                if (!offspringGenotype[i].Assigned)
                {
                    var itemInParent2 = parent2Cloned.Items[i];
                    offspringGenotype[i].Item = itemInParent2;
                    offspringGenotype[i].Assigned = true;
                }
            }

            return new TGenotype { Items = offspringGenotype.Select(i => i.Item).ToArray() };
        }

        class ItemAssigned
        {
            public TGene Item { get; set; }
            public bool Assigned { get; set; }
        }
    }
}
