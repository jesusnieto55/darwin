﻿using DarwinFramework.Utils;
using System.Collections.Generic;
using System.Linq;

namespace DarwinFramework.Recombination
{
    /// <summary>
    /// Implements the N Point crossover recombination strategy for two individuals.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    /// <typeparam name="TGene">Type of the gene</typeparam>
    public class NPointCrossover<TGenotype, TGene> : IRecombinationStrategy<TGenotype>
        where TGenotype : IPermutation<TGene>, ICloneable<TGenotype>, new()
    {
        private readonly RandomGenerator randomGenerator;
        private readonly int n;

        public NPointCrossover(RandomGenerator randomGenerator, int n)
        {
            this.randomGenerator = randomGenerator;
            this.n = n;
        }

        public IEnumerable<TGenotype> Recombine(TGenotype parent1, TGenotype parent2)
        {
            var crossoverPoints1 = this.GenerateCrossoverPoints(parent1);
            var crossoverPoints2 = this.GenerateCrossoverPoints(parent2);

            var offspring1 = this.CreateChild(parent1, parent2, crossoverPoints1, crossoverPoints2);
            var offspring2 = this.CreateChild(parent2, parent1, crossoverPoints2, crossoverPoints1);

            return new[] { offspring1, offspring2 };
        }

        private int[] GenerateCrossoverPoints(TGenotype parent)
            => Enumerable.Range(0, this.n)
                .Select(x => this.randomGenerator.Generate(0, parent.Items.Length - 1))
                .OrderBy(x => x).ToArray();

        private TGenotype CreateChild(TGenotype parent1, TGenotype parent2, int[] crossoverPoints1, int[] crossoverPoints2)
        {
            var parent1Cloned = parent1.Clone();
            var parent2Cloned = parent2.Clone();

            var takeFromFirstParent = true;
            var crossoverIndex1 = 0;
            var crossoverIndex2 = 0;
            var childGenotype = new List<TGene>();
            var i = 0;
            var keepLooping = true;

            while (keepLooping)
            {
                if (takeFromFirstParent
                    && crossoverIndex1 < crossoverPoints1.Length
                    && (i - 1) == crossoverPoints1[crossoverIndex1])
                {
                    crossoverIndex1++;
                    takeFromFirstParent = !takeFromFirstParent;
                    i = crossoverPoints2[crossoverIndex2] + 1;
                    crossoverIndex2++;
                }
                if (!takeFromFirstParent
                    && crossoverIndex2 < crossoverPoints2.Length
                    && (i - 1) == crossoverPoints2[crossoverIndex2])
                {
                    crossoverIndex2++;
                    takeFromFirstParent = !takeFromFirstParent;
                    i = crossoverPoints1[crossoverIndex1] + 1;
                    crossoverIndex1++;
                }
                if (takeFromFirstParent && i < parent1Cloned.Items.Length)
                {
                    childGenotype.Add(parent1Cloned.Items[i]);
                }
                else if (!takeFromFirstParent && i < parent2Cloned.Items.Length)
                {
                    childGenotype.Add(parent2Cloned.Items[i]);
                }
                i++;
                keepLooping = takeFromFirstParent ? i < parent1Cloned.Items.Length : i < parent2Cloned.Items.Length;
            }

            parent1Cloned.Items = childGenotype.ToArray();
            return parent1Cloned;
        }
    }
}
