﻿using System.Collections.Generic;

namespace DarwinFramework.Recombination
{
    /// <summary>
    /// Interface that defines the recombination strategy for couple of individuals
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    public interface IRecombinationStrategy<TGenotype>
    {
        IEnumerable<TGenotype> Recombine(TGenotype parent1, TGenotype parent2);
    }
}
