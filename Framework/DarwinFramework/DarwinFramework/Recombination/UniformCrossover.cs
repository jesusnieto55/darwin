﻿using DarwinFramework.Utils;
using System.Collections.Generic;

namespace DarwinFramework.Recombination
{
    /// <summary>
    /// Implements the Uniform crossover recombination strategy for two individuals.
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    /// <typeparam name="TGene">Type of the gene</typeparam>
    public class UniformCrossover<TGenotype, TGene> : IRecombinationStrategy<TGenotype>
        where TGenotype : IPermutation<TGene>, ICloneable<TGenotype>, new()
    {
        private readonly RandomGenerator randomGenerator;

        public UniformCrossover(RandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public IEnumerable<TGenotype> Recombine(TGenotype parent1, TGenotype parent2)
        {
            var parent1Cloned = parent1.Clone();
            var parent2Cloned = parent2.Clone();

            var offspring1Genotype = new List<TGene>();
            var offspring2Genotype = new List<TGene>();

            for (int i = 0; i < parent1.Items.Length; i++)
            {
                if (this.TakeFromFirstParent())
                {
                    offspring1Genotype.Add(parent1.Items[i]);
                    offspring2Genotype.Add(parent2.Items[i]);
                }
                else
                {
                    offspring2Genotype.Add(parent1.Items[i]);
                    offspring1Genotype.Add(parent2.Items[i]);
                }
            }

            parent1Cloned.Items = offspring1Genotype.ToArray();
            parent2Cloned.Items = offspring2Genotype.ToArray();

            return new TGenotype[] { parent1Cloned, parent2Cloned };
        }

        private bool TakeFromFirstParent()
            => this.randomGenerator.Generate(0, 9) < 5;
    }
}
