﻿namespace DarwinFramework
{
    public interface ICloneable<T>
    {
        T Clone();
    }
}
