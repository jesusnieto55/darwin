﻿using DarwinFramework.Individual;
using DarwinFramework.Utils;
using System;
using System.Linq;

namespace DarwinFramework.Mutation
{
    public class UncorrelatedMultiStepMutationStrategy<TGenotype> : IMutationStrategy<TGenotype> where TGenotype : IEvolutionaryIndividual, ICloneable<TGenotype>
    {
        private readonly double tau;
        private readonly double tau2;
        private readonly double epsilon;
        private readonly NormalDistribution normalDistribution;

        public UncorrelatedMultiStepMutationStrategy(double tau, double tau2, double epsilon, NormalDistribution normalDistribution)
        {
            this.tau = tau;
            this.tau2 = tau2;
            this.epsilon = epsilon;
            this.normalDistribution = normalDistribution;
        }

        public TGenotype Mutate(TGenotype individual)
        {
            if (individual.StrategyParameters.Length != individual.Genotype.Length)
            {
                throw new ArgumentException("Strategy Parameters must have the same length as the Genotype");
            }
            var mutatedIndividual = individual.Clone();
            mutatedIndividual.StrategyParameters = this.MutateStrategyParameters(individual.StrategyParameters);
            mutatedIndividual.Genotype = this.MutateGenotype(individual.Genotype, mutatedIndividual.StrategyParameters);

            return mutatedIndividual;
        }

        private double[] MutateStrategyParameters(double[] strategyParameters)
        {
            var randomNormal = this.normalDistribution.Generate(0, 1);
            return strategyParameters.Select(
                x =>
                {
                    var mutated = x * Math.Pow(Math.E, this.tau2 * randomNormal + this.tau * this.normalDistribution.Generate(0, 1));
                    if (Math.Abs(mutated) < this.epsilon)
                    {
                        mutated = this.epsilon;
                    }
                    return mutated;
                }
                ).ToArray();
        }

        private double[] MutateGenotype(double[] genotype, double[] strategyParameters)
        {
            return genotype.Select((x, index) => x + (strategyParameters[index] * this.normalDistribution.Generate(0, 1))).ToArray();
        }
    }
}
