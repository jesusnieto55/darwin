﻿using DarwinFramework.Mutation.MutationRate;
using DarwinFramework.Utils;

namespace DarwinFramework.Mutation
{
    /// <summary>
    /// Implements the mutation strategy called random resetting strategy for integer representations
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype, it must be a permutation of integers</typeparam>
    public class RandomResettingMutationStrategy<TGenotype> : IMutationStrategy<TGenotype>
        where TGenotype : IPermutation<int>, ICloneable<TGenotype>
    {
        private readonly IMutationRateStrategy<TGenotype> mutationRateStrategy;
        private readonly RandomGenerator randomGenerator;
        private readonly int maxMutated;

        public RandomResettingMutationStrategy(
            IMutationRateStrategy<TGenotype> mutationRateStrategy,
            RandomGenerator randomGenerator,
            int maxMutated)
        {
            this.mutationRateStrategy = mutationRateStrategy;
            this.randomGenerator = randomGenerator;
            this.maxMutated = maxMutated;
        }

        public TGenotype Mutate(TGenotype individual)
        {
            var mutated = individual.Clone();

            for (var i = 0; i < mutated.Items.Length; i++)
            {
                if (this.ShouldMutate(mutated))
                {
                    mutated.Items[i] = this.randomGenerator.Generate(0, this.maxMutated);
                }
            }

            return mutated;
        }

        private bool ShouldMutate(TGenotype individual)
        {
            var random = this.randomGenerator.Generate(0, 100);
            return random <= (this.mutationRateStrategy.GetMutationRate(individual) * 100);
        }
    }
}
