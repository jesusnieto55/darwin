﻿namespace DarwinFramework.Mutation
{
    public class MutationSettings
    {
        public double MutationRate { get; set; } = 0.5;
    }
}
