﻿using DarwinFramework.Mutation.MutationRate;
using DarwinFramework.Utils;

namespace DarwinFramework.Mutation
{
    /// <summary>
    /// Implements the mutation strategy called creep mutationi for integer representations
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype, it must be a permutation of integers</typeparam>
    public class CreepMutationStrategy<TGenotype> : IMutationStrategy<TGenotype>
        where TGenotype : IPermutation<int>, ICloneable<TGenotype>
    {
        private readonly IMutationRateStrategy<TGenotype> mutationRateStrategy;
        private readonly NormalDistribution normalDistribution;
        private readonly RandomGenerator randomGenerator;

        public CreepMutationStrategy(
            IMutationRateStrategy<TGenotype> mutationRateStrategy,
            NormalDistribution normalDistribution,
            RandomGenerator randomGenerator)
        {
            this.mutationRateStrategy = mutationRateStrategy;
            this.normalDistribution = normalDistribution;
            this.randomGenerator = randomGenerator;
        }

        public TGenotype Mutate(TGenotype individual)
        {
            var mutated = individual.Clone();

            for (var i = 0; i < mutated.Items.Length; i++)
            {
                if (this.ShouldMutate(mutated))
                {
                    var random = this.normalDistribution.Generate(0, 1);
                    var mutatedGene = mutated.Items[i] + random;
                    mutated.Items[i] = (int)System.Math.Round(mutatedGene);
                }
            }

            return mutated;
        }

        private bool ShouldMutate(TGenotype individual)
        {
            var random = this.randomGenerator.Generate(0, 100);
            return random <= (this.mutationRateStrategy.GetMutationRate(individual) * 100);
        }
    }
}
