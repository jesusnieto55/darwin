﻿using DarwinFramework.Individual;
using DarwinFramework.Utils;
using System;
using System.Linq;

namespace DarwinFramework.Mutation
{
    public class UncorrelatedOneStepMutationStrategy<TGenotype> : IMutationStrategy<TGenotype> where TGenotype : IEvolutionaryIndividual, ICloneable<TGenotype>
    {
        private readonly double tau;
        private readonly double epsilon;
        private readonly NormalDistribution normalDistribution;

        public UncorrelatedOneStepMutationStrategy(double tau, double epsilon, NormalDistribution normalDistribution)
        {
            this.tau = tau;
            this.epsilon = epsilon;
            this.normalDistribution = normalDistribution;
        }

        public TGenotype Mutate(TGenotype individual)
        {
            if (individual.StrategyParameters.Length != 1)
            {
                throw new ArgumentException("Strategy Parameters must have length 1: sigma");
            }
            var mutatedIndividual = individual.Clone();
            var sigma = individual.StrategyParameters.First();
            var mutatedSigma = this.MutateSigma(sigma);

            mutatedIndividual.StrategyParameters = new[] { mutatedSigma };
            mutatedIndividual.Genotype = mutatedIndividual.Genotype.Select(x => this.MutateGene(x, mutatedSigma)).ToArray();

            return mutatedIndividual;
        }

        private double MutateSigma(double sigma)
        {
            var random = this.normalDistribution.Generate(0, 1);
            var mutatedSigma = sigma * Math.Pow(Math.E, this.tau * random);
            if (Math.Abs(mutatedSigma) < this.epsilon)
            {
                mutatedSigma = this.epsilon;
            }
            return mutatedSigma;
        }

        private double MutateGene(double gene, double sigma)
        {
            var random = this.normalDistribution.Generate(0, 1);
            return gene + sigma*random;
        }
    }
}
