﻿using DarwinFramework.Utils;

namespace DarwinFramework.Mutation
{
    /// <summary>
    /// Implements the mutation strategy called swap mutation strategy.
    /// It will swap the position of two random items in the individual's genotype
    /// </summary>
    /// <typeparam name="TGenotype">Type of the genotype</typeparam>
    /// <typeparam name="TGene">Type of the gene</typeparam>
    public class SwapMutationStrategy<TGenotype, TGene> : IMutationStrategy<TGenotype>
        where TGenotype : IPermutation<TGene>, ICloneable<TGenotype>
    {
        private readonly MutationSettings settings;
        private readonly RandomGenerator randomGenerator;

        public SwapMutationStrategy(MutationSettings settings, RandomGenerator randomGenerator)
        {
            this.settings = settings;
            this.randomGenerator = randomGenerator;
        }

        public TGenotype Mutate(TGenotype individual)
        {
            if (!this.ShouldMutate())
            {
                return individual.Clone();
            }
            var position1 = this.randomGenerator.Generate(0, individual.Items.Length - 1);
            var position2 = this.randomGenerator.Generate(0, individual.Items.Length - 1);

            return this.ExchangePositions(individual, position1, position2);
        }

        private TGenotype ExchangePositions(TGenotype genotype, int position1, int position2)
        {
            var mutatedGenotype = genotype.Clone();
            var aux = mutatedGenotype.Items[position1];

            mutatedGenotype.Items[position1] = mutatedGenotype.Items[position2];
            mutatedGenotype.Items[position2] = aux;

            return mutatedGenotype;
        }

        private bool ShouldMutate()
        {
            var random = this.randomGenerator.Generate(0, 10);
            return random <= (this.settings.MutationRate * 10);
        }
    }
}
