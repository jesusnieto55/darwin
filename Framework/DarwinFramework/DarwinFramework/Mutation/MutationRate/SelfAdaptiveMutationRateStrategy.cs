﻿using DarwinFramework.Individual;
using DarwinFramework.Utils;

namespace DarwinFramework.Mutation.MutationRate
{
    public class SelfAdaptiveMutationRateStrategy<TGenotype> : IMutationRateStrategy<TGenotype>
        where TGenotype : SelfAdaptiveMutationRateIndividual
    {
        private readonly RandomGenerator randomGenerator;

        public SelfAdaptiveMutationRateStrategy(RandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public double GetMutationRate(TGenotype individual)
        {
            var mutationRate = individual.DecodeMutationRate();

            var mutatedEncodedMutationRate = this.Mutate(individual.EncodedMutationRate, mutationRate);
            individual.EncodedMutationRate = mutatedEncodedMutationRate;

            return individual.DecodeMutationRate();
        }

        private bool[] Mutate(bool[] encodedMutationRate, double mutationRate)
        {
            for (var i = 0; i < encodedMutationRate.Length; i++)
            {
                if (this.ShouldMutate(mutationRate))
                {
                    encodedMutationRate[i] = !encodedMutationRate[i];
                }
            }

            return encodedMutationRate;
        }

        private bool ShouldMutate(double mutationRate)
        {
            var random = this.randomGenerator.Generate(0, 10);
            return random <= (mutationRate * 10);
        }
    }
}
