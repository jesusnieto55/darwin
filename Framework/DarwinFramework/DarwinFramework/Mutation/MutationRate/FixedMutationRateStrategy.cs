﻿namespace DarwinFramework.Mutation.MutationRate
{
    public class FixedMutationRateStrategy<TGenotype> : IMutationRateStrategy<TGenotype>
    {
        private readonly double mutationRate;

        public FixedMutationRateStrategy(double mutationRate)
        {
            this.mutationRate = mutationRate;
        }

        public double GetMutationRate(TGenotype individual) => this.mutationRate;
    }
}
