﻿namespace DarwinFramework.Mutation.MutationRate
{
    public interface IMutationRateStrategy<TGenotype>
    {
        double GetMutationRate(TGenotype individual);
    }
}
