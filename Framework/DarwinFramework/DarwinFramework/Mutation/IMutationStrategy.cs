﻿namespace DarwinFramework.Mutation
{
    /// <summary>
    /// Interface that defines the mutation strategy for an individual represented by TGenotype
    /// </summary>
    /// <typeparam name="TGenotype">Type of the individual's genotype</typeparam>
    public interface IMutationStrategy<TGenotype>
    {
        TGenotype Mutate(TGenotype individual);
    }
}
