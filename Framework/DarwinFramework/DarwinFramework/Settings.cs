﻿namespace DarwinFramework
{
    public class Settings
    {
        public int PopulationSize { get; set; }
        public int NumberOfParents { get; set; }
        public double CrossoverRate { get; set; } = 0.5;
        public bool SaveHistory { get; set; }
    }
}
