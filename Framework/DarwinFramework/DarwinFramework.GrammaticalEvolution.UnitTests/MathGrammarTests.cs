using System.Linq;
using Xunit;

namespace DarwinFramework.GrammaticalEvolution.UnitTests
{
    public class MathGrammarTests
    {
        private readonly MathGrammar mathGrammar;
        private readonly int maxCodons = 100;

        public MathGrammarTests()
        {
            mathGrammar = new MathGrammar(this.maxCodons);
        }

        [Theory]
        [InlineData(new[] { 3, 0 }, "x")]
        [InlineData(new[] { 3, 1 }, "1")]
        [InlineData(new[] { 0, 3, 0, 0, 3, 1 }, "x+1")]
        [InlineData(new[] { 2, 1, 0, 3, 0, 0, 3, 1 }, "cos(x+1)")]
        [InlineData(new[] { 0, 1, 1, 3, 1, 0, 3, 1, 0, 3, 1, 2, 1, 3, 0, 2, 3, 0 }, "((1+1)+1)*(x*x)")]
        [InlineData(new[] { 0,1,1,3,1,0,3,1,0,1,3,1,0,3,1,3,1,1,1,3,0,0,3,1,0,3,1,2,1,1,3,0,0,3,1,0,3,1 }, "((1+1)+(1+1))/(((x+1)+1)*((x+1)+1))")]
        public void Can_Print_Expression(int[] codons, string expectedResult)
        {
            var result = this.mathGrammar.PrintExpression(codons.ToList());

            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new[] { 3, 1 }, 1, 1)] // 1
        [InlineData(new[] { 3, 0 }, 5, 5)] // x
        [InlineData(new[] { 0, 3, 0, 0, 3, 1 }, 14, 15)] // x+1
        [InlineData(new[] { 0, 15, 0, 0, 15, 5 }, 14, 15)] // x+1
        [InlineData(new[] { 2, 1, 0, 3, 0, 0, 3, 1 }, -1, 1)] // cos(x+1)
        [InlineData(new[] { 6, 5, 0, 7, 0, 0, 7, 5 }, -1, 1)] // cos(x+1)
        [InlineData(new[] { 0, 1, 1, 3, 1, 0, 3, 1, 0, 3, 1, 2, 1, 3, 0, 2, 3, 0 }, 5, 75)] // ((1+1)+1)*(x*x) = 3x^2
        [InlineData(new[] { 0, 1, 1, 3, 1, 0, 3, 1, 0, 1, 3, 1, 0, 3, 1, 3, 1, 1, 1, 3, 0, 0, 3, 1, 0, 3, 1, 2, 1, 1, 3, 0, 0, 3, 1, 0, 3, 1 }, 0, 1)] // 4/((x+2)^2)
        [InlineData(new[] { 0, 2, 3, 0, 3, 1, 0, 0, 0, 3, 1, 0, 3, 1, 2, 3, 0, 0, 0, 0, 0, 3, 1, 0, 3, 1, 2, 3, 0, 3, 0, 3, 1, 0, 0, 0, 3, 1, 0, 3, 1, 2, 3, 0 }, 0, 0)] // ln(1+2x) + 2x/(1+2x)
        [InlineData(new[] { 0, 2, 3, 0, 3, 1, 0, 0, 0, 3, 1, 0, 3, 1, 2, 3, 0, 0, 0, 0, 0, 3, 1, 0, 3, 1, 2, 3, 0, 3, 0, 3, 1, 0, 0, 0, 3, 1, 0, 3, 1, 2, 3, 0 }, 1, 1.7652789553)] // ln(1+2x) + 2x/(1+2x)
        [InlineData(new[] { 0, 3, 1, 3, 1, 1, 1, 3, 1, 0, 3, 1, 0, 1, 3, 1, 0, 3, 1, 0, 3, 1, 2, 1 }, 0, 0.2)] // 1/5
        [InlineData(new[] { 0, 1, 0, 1, 1, 3, 1, 0, 3, 1, 0, 3, 1, 2, 1, 3, 0, 2, 3, 0, 1, 0, 1, 3, 1, 0, 3, 1, 2, 3, 0, 0, 3, 1}, -2, 17)] // (3x^2-2x)+1
        [InlineData(new[] { 0, 0, 3, 1, 3, 1, 1, 1, 3, 1, 0, 3, 1, 0, 1, 3, 1, 0, 3, 1, 0, 3, 1, 2, 1, 1, 0, 1, 1, 3, 1, 0, 3, 1, 0, 3, 1, 2, 1, 3, 0, 2, 3, 0, 1, 0, 1, 3, 1, 0, 3, 1, 2, 3, 0, 0, 3, 1 }, -2, 3.4)] // (1/5)*(3x^2-2x+1)
        public void Can_Evaluate_Expression(int[] codons, double input, double expectedResult)
        {
            var result = this.mathGrammar.Evaluate(codons.ToList(), input);

            Assert.True(result.HasValue);
            Assert.Equal(expectedResult, result.Value, 10);
        }

        [Theory]
        [InlineData(new[] { 0 }, false)]
        [InlineData(new[] { 0, 1 }, false)]
        [InlineData(new[] { 0, 3, 1, 0 }, false)]
        [InlineData(new[] { 0, 3, 1, 3, 3, 0 }, true)] // 1/x
        [InlineData(new[] { 2, 3, 3, 0 }, true)] // ln(x)
        [InlineData(new[] { 0, 3, 0, 0, 3 }, true)]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, false)]
        public void Are_Codons_Valid(int[] codons, bool expectedValid)
        {
            var result = this.mathGrammar.IsValid(codons.ToList());

            Assert.Equal(expectedValid, result);
        }

        [Theory]
        [InlineData(new[] { 0 }, 1, false)]
        [InlineData(new[] { 0, 1 }, 1, false)]
        [InlineData(new[] { 0, 3, 1, 0 }, 1, false)]
        [InlineData(new[] { 0, 3, 1, 3, 3, 0 }, 1, true)] // 1/x
        [InlineData(new[] { 0, 3, 1, 3, 3, 0 }, 0, false)] // 1/x
        [InlineData(new[] { 2, 3, 3, 0 }, 1, true)] // ln(x)
        [InlineData(new[] { 2, 3, 3, 0 }, -1, false)] // ln(x)
        [InlineData(new[] { 0, 3, 0, 0, 3 }, 0, true)]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 0, false)]
        [InlineData(new[] { 0, 3, 1, 3, 2, 1, 3, 0 }, 0, true)] // 1/cos(x)
        [InlineData(new[] { 0, 3, 1, 3, 2, 3, 3, 0 }, 1, false)] // 1/ln(x)
        [InlineData(new[] { 0, 3, 1, 3, 2, 3, 3, 0 }, 10, true)] // 1/ln(x)
        public void Is_Expression_Valid(int[] codons, double input, bool expectedValid)
        {
            var result = this.mathGrammar.Evaluate(codons.ToList(), input);

            Assert.Equal(expectedValid, result.HasValue);
        }
    }
}
