﻿using DarwinFramework;
using System;

namespace TravellingSalesmanProblem
{
    /// <summary>
    /// It calculates the fitness of a TSP solution.
    /// The fitness will be the sum of the distances between the consecutives cities. Since we want to minimize this value,
    /// we return the inverse of this sum multiplied by a constant in order to avoid having very small results.
    /// </summary>
    public class TSPFitnessCalculator : FitnessCalculator<TSPSolution>
    {
        public override double GetFitness(TSPSolution solution)
        {
            double totalDistance = 0;

            for (var i = 0; i < solution.Items.Length; i++)
            {
                var origin = solution.Items[i];
                var destination = solution.Items[0];
                if (i + 1 < solution.Items.Length)
                {
                    destination = solution.Items[i + 1];
                }
                var distanceBetweenCities = this.CalculateDistance(origin, destination);
                totalDistance += distanceBetweenCities;
            }

            return (totalDistance == 0 ? totalDistance : 1/totalDistance) * 10000;
        }

        private double CalculateDistance(City origin, City destination)
        {
            return Math.Sqrt(Math.Pow(destination.X - origin.X, 2) + Math.Pow(destination.Y - origin.Y, 2));
        }
    }
}
