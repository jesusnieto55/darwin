﻿using DarwinFramework;

namespace TravellingSalesmanProblem
{
    /// <summary>
    /// Class that represents a City in the Travelling Salesman Problem
    /// </summary>
    public class City : ICloneable<City>
    {
        public string Name { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public override string ToString()
        {
            return $"{Name} ({X}-{Y})";
        }

        public City Clone()
        {
            return new City { Name = this.Name, X = this.X, Y = this.Y };
        }

        public override bool Equals(object obj)
        {
            var cityToCompare = obj as City;
            if (cityToCompare == null)
            {
                return false;
            }

            return this.X == cityToCompare.X && this.Y == cityToCompare.Y;
        }
    }
}
