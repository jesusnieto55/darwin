﻿using DarwinFramework;
using System.Linq;

namespace TravellingSalesmanProblem
{
    /// <summary>
    /// Represents a solution in the Travelling Salesman Problem. A solution consists on a list of cities in which the order
    /// represents the order in which they will be visited.
    /// </summary>
    public class TSPSolution : ICloneable<TSPSolution>, IPermutation<City>
    {
        public City[] Items { get; set; }

        public TSPSolution Clone()
        {
            return new TSPSolution
            {
                Items = this.Items.Select(c => c.Clone()).ToArray()
            };
        }
    }
}
