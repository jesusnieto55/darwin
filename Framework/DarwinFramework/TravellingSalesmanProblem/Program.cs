﻿using System;
using System.IO;
using System.Linq;

namespace TravellingSalesmanProblem
{
    /// <summary>
    /// Entry point of the Travelling Salesman Problem console application.
    ///     1. Reads the cities file name from the parameter of the console application
    ///     2. Creates an instance of the TravellingSalesmanProblemSolver
    ///     3. Prints the solution
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var fileName = GetFileName(args);
            if (fileName == null)
            {
                Console.WriteLine("Please pass a file name as parameter, this file should be in the Data folder.");
                return;
            }

            Console.WriteLine("Calculating solution...");
            var watch = System.Diagnostics.Stopwatch.StartNew();

            var travellingSalesManProblemSolver = new TravellingSalesmanProblemSolver();
            travellingSalesManProblemSolver.Solve(fileName);

            watch.Stop();
            PrintSolution(travellingSalesManProblemSolver, watch.ElapsedMilliseconds);
        }

        private static string GetFileName(string[] args)
        {
            if (!args.Any())
            {
                return null;
            }
            return args[0];
        }

        private static void PrintSolution(TravellingSalesmanProblemSolver tsp, long time)
        {
            var bestSolution = tsp.GeneticAlgorithm.Population.GetBestIndividual();
            Console.WriteLine($"Path (Fitness {bestSolution.FitnessValue}): {string.Join(" - ", bestSolution.Individual.Items.Select(i => i.ToString()))}");
            Console.WriteLine($"Number of generations: {tsp.GeneticAlgorithm.NumberOfGenerations}");
            Console.WriteLine($"Execution time: {time} miliseconds");

            File.AppendAllLines(@".\output_appended.csv", new[] { $"{tsp.GeneticAlgorithm.NumberOfGenerations}-{time}-{bestSolution.FitnessValue}-{tsp.settings.GeneticAlgorithmSettings.CrossoverRate}-{tsp.settings.SwapMutationSettings.MutationRate}" });
        }
    }
}
