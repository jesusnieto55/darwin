﻿using DarwinFramework;
using DarwinFramework.Mutation;
using DarwinFramework.OffspringGeneration;
using DarwinFramework.ParentSelector;
using DarwinFramework.Recombination;
using DarwinFramework.SurvivorSelector;
using DarwinFramework.Termination;
using DarwinFramework.Utils;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TravellingSalesmanProblem
{
    /// <summary>
    /// Class that represents the solver of the Travelling Salesman Problem. This class does the following:
    ///     1. Reads the cities from a file
    ///     2. Reads the settings from a settings file
    ///     3. Initializes a random population
    ///     4. Creates a new instnce of the GeneticAlgorithm with the used strategies (SwapMutationStrategy, PartiallyMappedCrossover,
    ///        FitnessCalculator, AgeBasedSurvivorSelector, TournamentSelection and FitnessImprovementUnderThreshold)
    /// </summary>
    public class TravellingSalesmanProblemSolver
    {
        private readonly RandomGenerator randomGenerator;
        public TravellingSalesmanProblemSettings settings;

        public GeneticAlgorithm<TSPSolution> GeneticAlgorithm { get; private set; }

        public TravellingSalesmanProblemSolver()
        {
            this.randomGenerator = new RandomGenerator();
        }

        public void Solve(string citiesFileName)
        {
            this.ReadSettings();
            var cities = this.ReadCities(citiesFileName);

            var population = this.GenerateRandomPopulation(cities);

            var mutationStrategy = new SwapMutationStrategy<TSPSolution, City>(this.settings.SwapMutationSettings, randomGenerator);
            var recombinationStrategy = new PartiallyMappedCrossover<TSPSolution, City>(randomGenerator);
            var crossoverRateStrategy = new FixedCrossoverRate<TSPSolution>(this.settings.GeneticAlgorithmSettings.CrossoverRate);
            var offspringGenerationStrategy = new SexualOffspringGenerationStrategy<TSPSolution>(randomGenerator, recombinationStrategy, crossoverRateStrategy);
            var fitnessCalculator = new TSPFitnessCalculator();
            var survivorSelectorStrategy = new AgeBasedSurvivorSelector<TSPSolution>(fitnessCalculator);
            var parentSelectorStrategy = new TournamentSelection<TSPSolution>(this.settings.TournamentSelectionSettings, randomGenerator);
            var terminationStrategy = new FitnessImprovementUnderThreshold<TSPSolution>(this.settings.TerminationSettings);

            this.GeneticAlgorithm = new GeneticAlgorithm<TSPSolution>(
                mutationStrategy,
                offspringGenerationStrategy,
                terminationStrategy,
                survivorSelectorStrategy,
                parentSelectorStrategy,
                fitnessCalculator,
                this.settings.GeneticAlgorithmSettings);

            this.GeneticAlgorithm.Start(population);
        }

        private void ReadSettings()
        {
            using (var r = new StreamReader("Settings/settings.json"))
            {
                string json = r.ReadToEnd();
                this.settings = JsonConvert.DeserializeObject<TravellingSalesmanProblemSettings>(json);
            }
        }

        private IEnumerable<TSPSolution> GenerateRandomPopulation(IEnumerable<City> cities)
        {
            var population = new List<TSPSolution>();

            for (var i = 0; i < this.settings.GeneticAlgorithmSettings.PopulationSize; i++)
            {
                var randomIndividual = this.GenerateRandomIndividual(cities);
                population.Add(randomIndividual);
            }

            return population;
        }

        private TSPSolution GenerateRandomIndividual(IEnumerable<City> cities)
        {
            var citiesToPick = cities.ToList();
            var genotype = new List<City>();

            while (citiesToPick.Count > 0)
            {
                var random = this.randomGenerator.Generate(0, citiesToPick.Count - 1);
                genotype.Add(citiesToPick[random]);

                citiesToPick.RemoveAt(random);
            }

            return new TSPSolution { Items = genotype.ToArray() };
        }

        private IEnumerable<City> ReadCities(string fileName)
        {
            var path = Path.Combine("./Data", fileName);
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                List<City> cities = JsonConvert.DeserializeObject<List<City>>(json);
                return cities;
            }
        }
    }
}
