﻿using DarwinFramework;
using DarwinFramework.Mutation;
using DarwinFramework.ParentSelector;
using DarwinFramework.Termination;
using System;
using System.Collections.Generic;
using System.Text;

namespace TravellingSalesmanProblem
{
    public class TravellingSalesmanProblemSettings
    {
        public TournamentSelectionSettings TournamentSelectionSettings { get; set; }
        public MutationSettings SwapMutationSettings { get; set; }
        public Settings GeneticAlgorithmSettings { get; set; }
        public FitnessImprovementUnderThresholdSettings TerminationSettings { get; set; }
    }
}
