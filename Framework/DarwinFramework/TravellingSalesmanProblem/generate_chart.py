﻿import matplotlib.pyplot as plt
import csv


def print_csv_result():
    x_values = []
    y_values = []
    with open('output.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                x_values.append(row[0])
                y_values.append(row[1])
            line_count += 1
    prev_y = 0
    gens_skipped = 0
    for i in range(len(x_values)):
        y = y_values[i]
        x = x_values[i]
        if prev_y != y or gens_skipped > 100:
            plt.scatter(float(x), float(y), 10, '#ff0f0f')
            gens_skipped = 0
        else:
            gens_skipped = gens_skipped + 1
        prev_y = y
    plt.show()


print('Starting')
print_csv_result()
print('Done!')