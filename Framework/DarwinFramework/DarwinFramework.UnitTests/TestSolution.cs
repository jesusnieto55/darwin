﻿using System.Linq;

namespace DarwinFramework.UnitTests
{
    public class TestSolution : IPermutation<int>, ICloneable<TestSolution>
    {
        public int[] Items { get; set; }

        public TestSolution Clone()
        {
            return new TestSolution { Items = this.Items.Select(i => i).ToArray() };
        }
    }
}
