﻿using Xunit;
using DarwinFramework.Utils;
using Moq;
using FluentAssertions;
using System.Linq;
using DarwinFramework.LocalSearch;
using System.Collections.Generic;

namespace DarwinFramework.UnitTests.LocalSearch
{
    public class GeneDuplicationTests
    {
        [Theory]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 1, 2, 1 }, new[] { 0, 1, 2, 1, 2, 3, 4, 5 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 0, 0, 0 }, new[] { 0, 1, 2, 3, 4, 5 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 1, 4, 1 }, new[] { 0, 1, 2, 3, 4, 1, 2, 3, 4, 5 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 1, 10, 4 }, new[] { 0, 1, 2, 3, 4, 5, 4, 5 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 1, 1, 6 }, new[] { 0, 1, 2, 3, 4, 5 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 1, 6, 0 }, new[] { 0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5 })]
        public void Duplicates_Genes(int[] genotype, int[] randomNumbers, int[] expectedChild1Genotype)
        {
            // Arrange
            var randomGeneratorMock = new Mock<RandomGenerator>();
            randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(randomNumbers[0])
                .Returns(randomNumbers[1])
                .Returns(randomNumbers[2]);
            var geneDuplication = new GeneDuplication<TestSolution, int>(randomGeneratorMock.Object);

            // Act
            var parent1 = new TestSolution { Items = genotype };
            var children = geneDuplication.ImproveOffspring(null, new List<TestSolution> { parent1 });

            // Assert
            var expectedChild = new TestSolution { Items = expectedChild1Genotype };
            children.Should().HaveCount(1);
            children.First().Should().BeEquivalentTo(expectedChild);
        }
    }
}
