﻿using DarwinFramework.Individual;
using DarwinFramework.SurvivorSelector;
using DarwinFramework.Utils;
using FluentAssertions;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DarwinFramework.UnitTests.SurvivorSelector
{
    public class AgeBasedSurvivorSelectorTests
    {
        private readonly Mock<FitnessCalculator<int[]>> fitnessCalculatorMock;
        private readonly AgeBasedSurvivorSelector<int[]> ageBasedSurvivorSelector;

        public AgeBasedSurvivorSelectorTests()
        {
            this.fitnessCalculatorMock = new Mock<FitnessCalculator<int[]>>();
            this.ageBasedSurvivorSelector = new AgeBasedSurvivorSelector<int[]>(this.fitnessCalculatorMock.Object);

            this.ArrangeFitnessCalculator();
        }

        [Fact]
        public async Task SelectSurvivors_BestInPopulationWorseThanOffspring_ReplaceAllPopulation()
        {
            // Arrange
            var individuals = new List<int[]>
            {
                new[] { 1, 2, 3 },
                new[] { 1, 1, 1 },
                new[] { 2, 2, 2 }
            };
            var offspring = new List<int[]>
            {
                new[] { 3, 3, 3 },
                new[] { 1, 1, 1 },
                new[] { 2, 2, 2 }
            };
            var population = new Population<int[]>(fitnessCalculatorMock.Object);
            population.SetIndividuals(individuals);

            // Act
            var newGeneration = this.ageBasedSurvivorSelector.SelectSurvivors(population, offspring);

            // Assert
            var expectedNewGeneration = offspring;
            newGeneration.Should().BeEquivalentTo(expectedNewGeneration);
        }

        [Fact]
        public async Task SelectSurvivors_BestInPopulationSameAsOffspring_ReplaceAllPopulation()
        {
            // Arrange
            var individuals = new List<int[]>
            {
                new[] { 1, 2, 3 },
                new[] { 1, 1, 1 },
                new[] { 3, 3, 3 }
            };
            var offspring = new List<int[]>
            {
                new[] { 3, 3, 3 },
                new[] { 1, 1, 1 },
                new[] { 2, 2, 2 }
            };
            var population = new Population<int[]>(fitnessCalculatorMock.Object);
            population.SetIndividuals(individuals);

            // Act
            var newGeneration = this.ageBasedSurvivorSelector.SelectSurvivors(population, offspring);

            // Assert
            var expectedNewGeneration = offspring;
            newGeneration.Should().BeEquivalentTo(expectedNewGeneration);
        }

        [Fact]
        public async Task SelectSurvivors_BestInPopulationBetterThanOffspring_ReplaceAllPopulationExceptOne()
        {
            // Arrange
            var individuals = new List<int[]>
            {
                new[] { 1, 2, 3 },
                new[] { 10, 10, 10 },
                new[] { 2, 2, 2 }
            };
            var offspring = new List<int[]>
            {
                new[] { 3, 3, 3 },
                new[] { 1, 1, 1 },
                new[] { 2, 2, 2 }
            };
            var population = new Population<int[]>(fitnessCalculatorMock.Object);
            population.SetIndividuals(individuals);

            // Act
            var newGeneration = this.ageBasedSurvivorSelector.SelectSurvivors(population, offspring);

            // Assert
            var expectedNewGeneration = new List<int[]>
            {
                new[] { 3, 3, 3 },
                new[] { 2, 2, 2 },
                new[] { 10, 10, 10 }
            }; ;
            newGeneration.Should().BeEquivalentTo(expectedNewGeneration);
        }

        private void ArrangeFitnessCalculator()
        {
            this.fitnessCalculatorMock
                .Setup(f => f.CalculateFitness(It.IsAny<int[]>()))
                .Returns((int[] param) => param.Sum());
        }
    }
}
