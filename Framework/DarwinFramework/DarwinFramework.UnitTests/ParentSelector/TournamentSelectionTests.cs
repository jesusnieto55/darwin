﻿using DarwinFramework.ParentSelector;
using DarwinFramework.Utils;
using FluentAssertions;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DarwinFramework.UnitTests.ParentSelector
{
    public class TournamentSelectionTests
    {
        private readonly Mock<RandomGenerator> randomGeneratorMock;
        private readonly Mock<FitnessCalculator<TestSolution>> fitnessCalculatorMock;
        private readonly TournamentSelection<TestSolution> tournamentSelection;

        public TournamentSelectionTests()
        {
            var settings = new TournamentSelectionSettings { TournamentSize = 2 };
            this.randomGeneratorMock = new Mock<RandomGenerator>();
            this.fitnessCalculatorMock = new Mock<FitnessCalculator<TestSolution>>();
            this.tournamentSelection = new TournamentSelection<TestSolution>(settings, randomGeneratorMock.Object);

            ArrangeFitnessCalculator();
        }

        [Fact]
        public async Task SelectParents_TwoParents_Case1()
        {
            // Arrange
            var individuals = new List<int[]>
            {
                new[] { 1, 2, 3 },
                new[] { 1, 1, 1 },
                new[] { 2, 2, 2 },
                new[] { 3, 3, 3 },
                new[] { 2, 1, 0 }
            };
            var population = new Population<TestSolution>(this.fitnessCalculatorMock.Object);
            population.SetIndividuals(individuals.Select(s => new TestSolution { Items = s } ));
            var numberOfParents = 2;
            this.ArrangeRandomGenerator();

            // Act
            var parents = tournamentSelection.SelectParents(population, numberOfParents).ToList();

            // Assert
            var expectedParents = new List<TestSolution>
            {
                new TestSolution { Items = new[] { 1, 2, 3 } },
                new TestSolution { Items = new[] { 1, 2, 3 } }
            };
            parents.Should().HaveCount(numberOfParents);
            parents.Should().BeEquivalentTo(expectedParents);
        }

        [Fact]
        public async Task SelectParents_TwoParents_Case2()
        {
            // Arrange
            var individuals = new List<int[]>
            {
                new[] { 1, 2, 3 },
                new[] { 1, 1, 1 },
                new[] { 2, 2, 2 },
                new[] { 3, 3, 3 },
                new[] { 2, 1, 0 }
            };
            var population = new Population<TestSolution>(this.fitnessCalculatorMock.Object);
            population.SetIndividuals(individuals.Select(s => new TestSolution { Items = s }));
            var numberOfParents = 2;
            this.ArrangeRandomGenerator(1, 2);

            // Act
            var parents = tournamentSelection.SelectParents(population, numberOfParents).ToList();

            // Assert
            var expectedParents = new List<TestSolution>
            {
                new TestSolution { Items = new[] { 2, 2, 2 } },
               new TestSolution { Items = new[] { 2, 2, 2 } }
            };
            parents.Should().HaveCount(numberOfParents);
            parents.Should().BeEquivalentTo(expectedParents);
        }

        [Fact]
        public async Task SelectParents_OneParent()
        {
            // Arrange
            var individuals = new List<int[]>
            {
                new[] { 0, 0, 0 },
                new[] { 0, 1, 0 }
            };
            var population = new Population<TestSolution>(this.fitnessCalculatorMock.Object);
            var numberOfParents = 1;
            population.SetIndividuals(individuals.Select(s => new TestSolution { Items = s }));
            this.ArrangeRandomGenerator();

            // Act
            var parents = tournamentSelection.SelectParents(population, numberOfParents).ToList();

            // Assert
            var expectedParents = new TestSolution
            {
                Items = new int[] { 0, 1, 0 }
            };
            parents.Should().HaveCount(numberOfParents);
            parents.Should().BeEquivalentTo(expectedParents);
        }

        private void ArrangeFitnessCalculator()
        {
            this.fitnessCalculatorMock
                .Setup(f => f.CalculateFitness(It.IsAny<TestSolution>()))
                .Returns((TestSolution param) => param.Items.Sum());
        }

        private void ArrangeRandomGenerator(int random1 = 0, int random2 = 1)
        {
            this.randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(random1)
                .Returns(random2)
                .Returns(random1)
                .Returns(random2)
                .Returns(random1)
                .Returns(random2);
        }
    }
}
