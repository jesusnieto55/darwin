﻿using Xunit;
using DarwinFramework.Recombination;
using System.Threading.Tasks;
using DarwinFramework.Utils;
using Moq;
using FluentAssertions;
using System.Linq;

namespace DarwinFramework.UnitTests.Recombination
{
    public class PartiallyMappedCrossoverTests
    {
        [Fact]
        public async Task Recombine_WithIntegerRepresentation_Case1()
        {
            var parent1 = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var parent2 = new[] { 9, 3, 7, 8, 2, 6, 5, 1, 4 };
            var expectedChild1 = new[] { 9, 3, 2, 4, 5, 6, 7, 1, 8 };

            TestPartiallyMappedCrossover(parent1, parent2, 3, 6, expectedChild1);
        }

        [Fact]
        public async Task Recombine_WithIntegerRepresentation_Case2()
        {
            var parent1 = new[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            var parent2 = new[] { 3, 7, 5, 1, 6, 8, 2, 4 };
            var expectedChild1 = new[] { 3, 7, 8, 4, 5, 6, 2, 1 };

            TestPartiallyMappedCrossover(parent1, parent2, 3, 5, expectedChild1);
        }

        [Fact]
        public async Task Recombine_WithIntegerRepresentation_Case3()
        {
            var parent1 = new[] { 8, 4, 7, 3, 6, 2, 5, 1, 9, 0 };
            var parent2 = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var expectedChild1 = new[] { 0, 7, 4, 3, 6, 2, 5, 1, 8, 9 };

            TestPartiallyMappedCrossover(parent1, parent2, 3, 7, expectedChild1);
        }

        private void TestPartiallyMappedCrossover(
            int[] parent1Genotype,
            int[] parent2Genotype,
            int crossoverPoint1,
            int crossoverPoint2,
            int[] expectedChildGenotype)
        {
            // Arrange
            var randomGeneratorMock = new Mock<RandomGenerator>();
            randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(crossoverPoint1)
                .Returns(crossoverPoint2);
            var recombinationStrategy = new PartiallyMappedCrossover<TestSolution, int>(randomGeneratorMock.Object);

            // Act
            var parent1 = new TestSolution { Items = parent1Genotype };
            var parent2 = new TestSolution { Items = parent2Genotype };
            var children = recombinationStrategy.Recombine(parent1, parent2);

            // Assert
            var expectedChild = new TestSolution { Items = expectedChildGenotype };
            children.Should().HaveCount(2);
            children.First().Should().BeEquivalentTo(expectedChild);
        }
    }
}
