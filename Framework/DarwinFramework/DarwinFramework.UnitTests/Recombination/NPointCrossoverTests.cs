﻿using Xunit;
using DarwinFramework.Recombination;
using System.Threading.Tasks;
using DarwinFramework.Utils;
using Moq;
using FluentAssertions;
using System.Linq;

namespace DarwinFramework.UnitTests.Recombination
{
    public class NPointCrossoverTests
    {
        [Theory]
        [InlineData(new[] { 1, 2, 3, 4, 5, 6, 7, 8 }, new[] { 9, 10, 11, 12, 13, 14 }, new[] { 1, 4 }, new[] { 0, 2 }, new[] { 1, 2, 10, 11, 6, 7, 8 }, new[] { 9, 3, 4, 5, 12, 13, 14 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 6, 7, 8, 9, 10, 11 }, new[] { 1, 4 }, new[] { 0, 2 }, new[] { 0, 1, 7, 8, 5 }, new[] { 6, 2, 3, 4, 9, 10, 11 })]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, new[] { 6, 7, 8, 9, 10, 11 }, new[] { 1, 2 }, new[] { 1, 2 }, new[] { 0, 1, 8, 3, 4, 5 }, new[] { 6, 7, 2, 9, 10, 11 })]
        [InlineData(new[] { 0, 1, 2, 3 }, new[] { 4, 5, 6, 7, 8, 9, 10, 11 }, new[] { 0, 1 }, new[] { 3, 6 }, new[] { 0, 8, 9, 10, 2, 3 }, new[] { 4, 5, 6, 7, 1, 11 })]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 0, 0 }, new[] { 0, 0 }, new[] { 0, 1, 2 }, new[] { 3, 4, 5 })]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 0, 2 }, new[] { 0, 2 }, new[] { 0, 4, 5 }, new[] { 3, 1, 2 })]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 0, 1 }, new[] { 0, 1 }, new[] { 0, 4, 2 }, new[] { 3, 1, 5 })]
        public async Task Can_Recombine(int[] parent1Genotype, int[] parent2Genotype, int[] crossoverPoints1, int[] crossoverPoints2, int[] expectedChild1Genotype, int[] expectedChild2Genotype)
        {
            // Arrange
            var randomGeneratorMock = new Mock<RandomGenerator>();
            randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(crossoverPoints1[0])
                .Returns(crossoverPoints1[1])
                .Returns(crossoverPoints2[0])
                .Returns(crossoverPoints2[1]);
            var recombinationStrategy = new NPointCrossover<TestSolution, int>(randomGeneratorMock.Object, 2);

            // Act
            var parent1 = new TestSolution { Items = parent1Genotype };
            var parent2 = new TestSolution { Items = parent2Genotype };
            var children = recombinationStrategy.Recombine(parent1, parent2);

            // Assert
            var expectedChild1 = new TestSolution { Items = expectedChild1Genotype };
            var expectedChild2 = new TestSolution { Items = expectedChild2Genotype };
            children.Should().HaveCount(2);
            children.First().Should().BeEquivalentTo(expectedChild1);
            children.ToList()[1].Should().BeEquivalentTo(expectedChild2);
        }
    }
}
