﻿using Xunit;
using DarwinFramework.Recombination;
using System.Threading.Tasks;
using DarwinFramework.Utils;
using Moq;
using FluentAssertions;
using System.Linq;

namespace DarwinFramework.UnitTests.Recombination
{
    public class UniformCrossoverTests
    {
        [Theory]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 0, 0, 0, 0 }, new[] { 0, 1, 2 }, new[] { 3, 4, 5 })]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 9, 9, 9, 9 }, new[] { 3, 4, 5 }, new[] { 0, 1, 2 })]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 0, 6, 2, 7 }, new[] { 0, 4, 2 }, new[] { 3, 1, 5 })]
        [InlineData(new[] { 0, 1, 2 }, new[] { 3, 4, 5 }, new[] { 0, 2, 9, 7 }, new[] { 0, 1, 5 }, new[] { 3, 4, 2 })]
        [InlineData(new[] { 0, 1, 2, 3 }, new[] { 4, 5, 6, 7 }, new[] { 5, 2, 2, 7 }, new[] { 4, 1, 2, 7 }, new[] { 0, 5, 6, 3 })]
        public async Task Can_Recombine(int[] parent1Genotype, int[] parent2Genotype, int[] randomNumbers, int[] expectedChild1Genotype, int[] expectedChild2Genotype)
        {
            // Arrange
            var randomGeneratorMock = new Mock<RandomGenerator>();
            randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(randomNumbers[0])
                .Returns(randomNumbers[1])
                .Returns(randomNumbers[2])
                .Returns(randomNumbers[3]);
            var recombinationStrategy = new UniformCrossover<TestSolution, int>(randomGeneratorMock.Object);

            // Act
            var parent1 = new TestSolution { Items = parent1Genotype };
            var parent2 = new TestSolution { Items = parent2Genotype };
            var children = recombinationStrategy.Recombine(parent1, parent2);

            // Assert
            var expectedChild1 = new TestSolution { Items = expectedChild1Genotype };
            var expectedChild2 = new TestSolution { Items = expectedChild2Genotype };
            children.Should().HaveCount(2);
            children.First().Should().BeEquivalentTo(expectedChild1);
            children.ToList()[1].Should().BeEquivalentTo(expectedChild2);
        }
    }
}
