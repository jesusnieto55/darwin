﻿using DarwinFramework.Mutation;
using DarwinFramework.Mutation.MutationRate;
using DarwinFramework.Utils;
using FluentAssertions;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace DarwinFramework.UnitTests.Mutation
{
    public class RandomResettingMutationStrategyTests
    {
        private readonly Mock<RandomGenerator> randomGeneratorMock;
        private readonly Mock<IMutationRateStrategy<TestSolution>> mutationRateStrategyMock;
        private readonly RandomResettingMutationStrategy<TestSolution> randomResettingMutation;
        private readonly double mutationRate = 0.5;

        public RandomResettingMutationStrategyTests()
        {
            this.randomGeneratorMock = new Mock<RandomGenerator>();
            this.mutationRateStrategyMock = new Mock<IMutationRateStrategy<TestSolution>>();

            this.mutationRateStrategyMock.Setup(m => m.GetMutationRate(It.IsAny<TestSolution>())).Returns(this.mutationRate);

            this.randomResettingMutation = new RandomResettingMutationStrategy<TestSolution>(this.mutationRateStrategyMock.Object, randomGeneratorMock.Object, 100);
        }

        [Theory]
        [InlineData(new[] { 1, 2, 3 }, new[] { 60, 70, 20, 10, 0 }, new[] { 1, 2, 10 })]
        [InlineData(new[] { 1, 2, 3 }, new[] { 20, 7, 20, 10, 80 }, new[] { 7, 10, 3 })]
        [InlineData(new[] { 1, 2, 3 }, new[] { 70, 70, 70, 70, 70 }, new[] { 1, 2, 3 })]
        [InlineData(new[] { 1, 2 }, new[] { 1, 2, 2, 3, 7 }, new[] { 2, 3 })]
        public async Task Mutate(int[] genotype, int[] randomNumbers, int[] expectedMutated)
        {
            // Arrange
            this.ArrangeRandomGenerator(randomNumbers);

            // Act
            var mutated = this.randomResettingMutation.Mutate(new TestSolution { Items = genotype });

            // Assert
            mutated.Should().BeEquivalentTo(new TestSolution { Items = expectedMutated });
        }

        private void ArrangeRandomGenerator(int[] randomNumbers)
        {
            this.randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(randomNumbers[0])
                .Returns(randomNumbers[1])
                .Returns(randomNumbers[2])
                .Returns(randomNumbers[3])
                .Returns(randomNumbers[4]);
        }
    }
}
