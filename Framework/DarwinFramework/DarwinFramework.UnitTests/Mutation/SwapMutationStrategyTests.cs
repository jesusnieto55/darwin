﻿using DarwinFramework.Mutation;
using DarwinFramework.Utils;
using FluentAssertions;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace DarwinFramework.UnitTests.Mutation
{
    public class SwapMutationStrategyTests
    {
        private readonly Mock<RandomGenerator> randomGeneratorMock;
        private readonly SwapMutationStrategy<TestSolution, int> swapMutationStrategy;
        private readonly MutationSettings settings;

        public SwapMutationStrategyTests()
        {
            this.settings = new MutationSettings();
            this.randomGeneratorMock = new Mock<RandomGenerator>();
            this.swapMutationStrategy = new SwapMutationStrategy<TestSolution, int>(this.settings, randomGeneratorMock.Object);
        }

        [Theory]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, new[] { 2, 1, 3, 4, 5 }, 0, 1)]
        [InlineData(new[] { 5, 2, 3, 4, 1 }, new[] { 2, 1, 3, 4, 5 }, 0, 4)]
        [InlineData(new[] { 1 }, new[] { 1 }, 0, 0)]
        [InlineData(new[] { 1, 2, 4, 3, 5 }, new[] { 2, 1, 3, 4, 5 }, 2, 3)]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, new[] { 1, 2, 3, 4, 5 }, 4, 4)]
        [InlineData(new[] { 1, 2, 5, 4, 3 }, new[] { 2, 1, 3, 4, 5 }, 2, 4)]
        public async Task Mutate(int[] genotype, int[] expectedMutated, int position1, int position2)
        {
            // Arrange
            this.ArrangeRandomGenerator(position1, position2);

            // Act
            var mutated = this.swapMutationStrategy.Mutate(new TestSolution { Items = genotype });

            // Assert
            mutated.Should().BeEquivalentTo(new TestSolution { Items = expectedMutated });
        }

        [Fact]
        public async Task ShouldNotMutate()
        {
            // Arrange
            var genotype = new[] { 1, 2, 5, 4, 3 };
            this.settings.MutationRate = 0;
            this.ArrangeRandomGenerator(0, 0);

            // Act
            var mutated = this.swapMutationStrategy.Mutate(new TestSolution { Items = genotype });

            // Assert
            mutated.Should().BeEquivalentTo(new TestSolution { Items = genotype });
        }

        private void ArrangeRandomGenerator(int random1 = 0, int random2 = 1)
        {
            this.randomGeneratorMock
                .SetupSequence(r => r.Generate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(random1)
                .Returns(random2);
        }
    }
}
