﻿using FluentAssertions;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DarwinFramework.UnitTests
{
    public class PopulationTests
    {
        private readonly Population<int[]> population;
        private readonly Mock<FitnessCalculator<int[]>> fitnessCalculatorMock;

        public PopulationTests()
        {
            this.fitnessCalculatorMock = new Mock<FitnessCalculator<int[]>>();
            this.population = new Population<int[]>(this.fitnessCalculatorMock.Object);
            this.ArrangeFitnessCalculator();
        }

        [Fact]
        public void GetBestIndividual_ShouldReturnBest()
        {
            // Arrange
            var population = new List<int[]>
            {
                new[] { 1, 2, 3 },
                new[] { 1, 1, 1 },
                new[] { 10, 10, 3 },
                new[] { 9, 5, 1 },
                new[] { 15, 0, 0 },
                new[] { 3, 3, 3 },
                new[] { 6, 7, 8 },
                new[] { 3, 3, 9 },
                new[] { 4, 1, 6 },
                new[] { 0, 1, 21 },
            };
            this.population.SetIndividuals(population);

            // Act
            var best = this.population.GetBestIndividual();

            // Assert
            best.Individual.Should().BeEquivalentTo(population[2]);
        }

        private void ArrangeFitnessCalculator()
        {
            this.fitnessCalculatorMock
                .Setup(f => f.CalculateFitness(It.IsAny<int[]>()))
                .Returns((int[] param) => param.Sum());
        }
    }
}
