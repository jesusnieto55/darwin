﻿import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d


def plot_3d_function(function, init=-100, end=100, title=''):
    x = np.linspace(init, end, 30)
    y = np.linspace(init, end, 30)

    X, Y = np.meshgrid(x, y)
    Z = function(X, Y)

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.contour3D(X, Y, Z, 100, cmap='viridis')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.title(title)

    plt.show()


def f_esfera(x, y):
    return (x - 10)**2 + (y - 10)**2


def f_schwefel(x, y):
    return 418.9829*2 + ((-x * np.sin(np.sqrt(np.absolute(x)))) + (-y * np.sin(np.sqrt(np.absolute(y)))))


plot_3d_function(f_esfera, title='Esfera desplazada n=2')
plot_3d_function(f_schwefel, init=-500, end=500, title='Schwefel n=2')
