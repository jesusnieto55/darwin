﻿using DarwinFramework;
using System;
using System.Linq;

namespace Functions
{
    public class SchwefelFitnessCalculator : FitnessCalculator<FunctionSolution>
    {
        public override double GetFitness(FunctionSolution individual)
        {
            var n = individual.Genotype.Length;
            var value = 418.9829 * n + individual.Genotype.Sum(x => -1*x*Math.Sin(Math.Sqrt(Math.Abs(x))));

            return -1 * value;
        }
    }
}
