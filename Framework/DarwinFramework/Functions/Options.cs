﻿using CommandLine;
using System;

namespace Functions
{
    public class Options
    {
        [Option('n', "Dimension", Required = false, HelpText = "Dimension of the function", Default = 10)]
        public int Dimension { get; set; }

        [Option('p', "PopulationSize", Required = false, HelpText = "Size of the population", Default = 10)]
        public int PopulationSize { get; set; }

        [Option('g', "MaxGenerations", Required = false, HelpText = "Maximum number of generations", Default = 2000)]
        public int MaxGenerations { get; set; }

        [Option('o', "OffspringSize", Required = false, HelpText = "Size of the offspring", Default = 200)]
        public int NumberOfChildren { get; set; }

        [Option('c', "CommaSurvivorSelection", Required = false, HelpText = "True to use Comma Survivor Selection, False tu use union")]
        public bool CommaSurvivorSelection { get; set; }

        [Option('s', "OneStepMutation", Required = false, HelpText = "True to use One Step Mutation, False tu Multi Step Mutation")]
        public bool OneStepMutation { get; set; }

        [Option('f', "Function", Required = true, HelpText = "Function to Minimize: Sphere or Schwefel")]
        public Functions Function { get; set; }

        public double Tau => 1 / Math.Sqrt(this.Dimension);
        public double Tau2 => 1 / (Math.Sqrt(2 * Math.Sqrt(this.Dimension)));
        public double Epsilon => 0.0001;
    }
}
