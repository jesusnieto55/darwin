﻿using DarwinFramework;
using DarwinFramework.Individual;
using System.Linq;

namespace Functions
{
    public class FunctionSolution : IEvolutionaryIndividual, ICloneable<FunctionSolution>
    {
        public double[] StrategyParameters { get; set; }
        public double[] Genotype { get; set; }

        public FunctionSolution Clone()
        {
            return new FunctionSolution
            {
                Genotype = Genotype.Select(c => c).ToArray(),
                StrategyParameters = StrategyParameters.Select(s => s).ToArray()
            };
        }

        public override string ToString()
        {
            return $"({string.Join(",", this.Genotype)})";
        }
    }
}
