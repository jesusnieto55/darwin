﻿using DarwinFramework;
using DarwinFramework.Mutation;
using DarwinFramework.OffspringGeneration;
using DarwinFramework.ParentSelector;
using DarwinFramework.SurvivorSelector;
using DarwinFramework.Termination;
using DarwinFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Functions
{
    public class FunctionProblemSolver
    {
        public GeneticAlgorithm<FunctionSolution> GeneticAlgorithm { get; private set; }
        private Options options;
        private readonly RandomGenerator randomGenerator;
        private readonly NormalDistribution normalDistribution;

        public FunctionProblemSolver()
        {
            this.randomGenerator = new RandomGenerator();
            this.normalDistribution = new NormalDistribution(this.randomGenerator);
        }

        public void Solve(Options options)
        {
            this.options = options;
            var population = this.GenerateRandomPopulation();

            var mutationStrategy = this.GetMutationStrategy();
            var offspringGenerationStrategy = new GlobalEvolutionaryOffspringStrategy<FunctionSolution>(this.randomGenerator, this.options.NumberOfChildren);
            var fitnessCalculator = this.GetFitnessCalculator();
            var survivorSelectorStrategy = new BestFitnessSurvivorSelector<FunctionSolution>(fitnessCalculator, this.options.CommaSurvivorSelection);
            var parentSelectorStrategy = new EvolutionaryParentSelection<FunctionSolution>();
            var terminationStrategy = new SolutionReachedTerminationStrategy<FunctionSolution>(this.options.MaxGenerations, 0, 0);// 0.000127278);

            var settings = new Settings
            {
                SaveHistory = true
            };

            this.GeneticAlgorithm = new GeneticAlgorithm<FunctionSolution>(
                mutationStrategy,
                offspringGenerationStrategy,
                terminationStrategy,
                survivorSelectorStrategy,
                parentSelectorStrategy,
                fitnessCalculator,
                settings);

            this.GeneticAlgorithm.Start(population);
        }

        private FitnessCalculator<FunctionSolution> GetFitnessCalculator()
        {
            switch(this.options.Function)
            {
                case Functions.Sphere:
                    return new SphereFitnessCalculator();
                case Functions.Schwefel:
                    return new SchwefelFitnessCalculator();
                default:
                    throw new ArgumentException($"Unknown function {this.options.Function}");
            }
        }

        private IMutationStrategy<FunctionSolution> GetMutationStrategy()
        {
            if (this.options.OneStepMutation)
            {
                return new UncorrelatedOneStepMutationStrategy<FunctionSolution>(this.options.Tau, this.options.Epsilon, this.normalDistribution);
            }
            return new UncorrelatedMultiStepMutationStrategy<FunctionSolution>(this.options.Tau, this.options.Tau2, this.options.Epsilon, this.normalDistribution);
        }

        private IEnumerable<FunctionSolution> GenerateRandomPopulation()
        {
            var domain = this.options.Function == Functions.Sphere ? 100 : 500;
            var population = new List<FunctionSolution>();

            for (var i = 0; i < this.options.PopulationSize; i++)
            {
                var randomIndividual = new FunctionSolution
                {
                    Genotype = Enumerable.Range(0, this.options.Dimension)
                            .Select(x => (double)this.randomGenerator.Generate(-domain, domain)).ToArray(),
                    StrategyParameters = Enumerable.Range(0, this.options.OneStepMutation ? 1 : this.options.Dimension)
                            .Select(x => this.normalDistribution.Generate(0, 1)).ToArray()
                };
                population.Add(randomIndividual);
            }

            return population;
        }
    }
}
