﻿using DarwinFramework;
using System;
using System.Linq;

namespace Functions
{
    public class SphereFitnessCalculator : FitnessCalculator<FunctionSolution>
    {
        public override double GetFitness(FunctionSolution individual)
        {
            var value = individual.Genotype.Sum(x => (Math.Pow(x - 10, 2)));
            return -1 * value;
        }
    }
}
