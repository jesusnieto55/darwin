﻿using CommandLine;
using System;
using System.Linq;

namespace Functions
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Parser.Default.ParseArguments<Options>(args)
                .MapResult(options =>
                {
                    Console.WriteLine("Calculating solution...");

                    var functionProblemSolver = new FunctionProblemSolver();
                    functionProblemSolver.Solve(options);

                    PrintSolution(functionProblemSolver);

                    return 0;
                }, _ => 1);
        }

        private static void PrintSolution(FunctionProblemSolver functionProblem)
        {
            var bestSolution = functionProblem.GeneticAlgorithm.Population.GetBestIndividual();
            Console.WriteLine($"Minimum {-1 * bestSolution.FitnessValue}: ({string.Join(" , ", bestSolution.Individual.Genotype.Select(i => i.ToString()))})");
            Console.WriteLine($"Number of generations: {functionProblem.GeneticAlgorithm.NumberOfGenerations}");
            Console.WriteLine($"Execution time: {functionProblem.GeneticAlgorithm.History.ExecutionTime} miliseconds");
        }
    }
}
